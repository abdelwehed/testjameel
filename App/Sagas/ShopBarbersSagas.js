import { call, put } from 'redux-saga/effects';
import ShopBarbersActions from '../Redux/ShopBarbersRedux';

export function * getShopBarbers (api, action) {
  const { shopId } = action;

  const response = yield call(api.getShopsBarbers, shopId);

  if (response.ok) {
    yield put(ShopBarbersActions.getShopBarbersSuccess(response.data))
  } else {
    yield put(ShopBarbersActions.getShopBarbersFailure())
  }
}