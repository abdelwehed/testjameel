import { call, put } from 'redux-saga/effects';
import ProfileActions from '../Redux/ProfileRedux';

export function * getProfileDetails (api, action) {
  const { userId } = action;

  const response = yield call(api.getProfile, userId);
  console.log({ RESPONSE: response})

  if (response.ok) {
    yield put(ProfileActions.getProfileSuccess(response.data))
  } else {
    yield put(ProfileActions.getProfileFailure())
  }
}
