import { call, put } from 'redux-saga/effects';
import SearchActions from '../Redux/SearchRedux';

export function * searchByShop (api, action) {
  const { searchTerm } = action;

  const response = yield call(api.getShopsList, searchTerm);
  
  if (response.ok) {
    yield put(SearchActions.searchSuccess(response.data))
  } else {
    yield put(SearchActions.searchFailure())
  }
}
