
import { call, put } from 'redux-saga/effects'
import RNRestart from 'react-native-restart'
import { AsyncStorage, I18nManager, Alert } from 'react-native';
import SettingsActions from '../Redux/SettingsRedux'
// import { SettingsSelectors } from '../Redux/SettingsRedux'

import I18n from 'react-native-i18n'

import { NavigationActions } from 'react-navigation'

export function* updateLanguage(action) {
  const {language, isRtl} = action
  I18n.locale = language

  this._storeLanguage(language)
}

_storeLanguage = async (language) => {

  
  await AsyncStorage.setItem('jammelLanguage', language, () => {
    setTimeout(() => {
      I18nManager.forceRTL(false);
      RNRestart.Restart();
    }, 200);
  })
}