import { call, put } from 'redux-saga/effects';
import AddBookingActions from '../Redux/AddBookingRedux';

export function * addBooking (api, action) {
  const { params } = action;

  const response = yield call(api.addBooking, params);
  console.log({ SAGAS_ADD_BOOKING: response.data, OK: response.ok });

  if (response.ok) {
    yield put(AddBookingActions.addBookingSuccess(response.ok))
  } else {
    yield put(AddBookingActions.addBookingFailure(response.ok))
  }
}
