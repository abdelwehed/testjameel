import { call, put } from 'redux-saga/effects';
import UpdateProfileActions from '../Redux/UpdateProfileRedux';

export function * updateProfile (api, action) {
  const { userId, params } = action;

  const response = yield call(api.updateProfile, userId, params);

  if (response.ok) {
    yield put(UpdateProfileActions.updateProfileSuccess(response.data))
  } else {
    yield put(UpdateProfileActions.updateProfileFailure())
  }
}
