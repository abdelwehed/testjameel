import { put, select } from 'redux-saga/effects'
import SettingsActions from '../Redux/SettingsRedux'
import { is } from 'ramda'
import { NavigationActions } from 'react-navigation'
import {Alert } from 'react-native';
import I18n from 'react-native-i18n'


export const selectLanguage = state => state.settings.language // get the language from the settings reducer
export const SelectLogin = state => state.login;

// process STARTUP actions
export function* startup(action) {

  const language = yield select(selectLanguage)

  I18n.locale = language

  //yield put(SettingsActions.changeLanguage(language))

  // only get if we don't have it yet
  const author = yield select(SelectLogin)
  
  yield put(NavigationActions.navigate({ routeName: 'AuthLoading' }))
  

}