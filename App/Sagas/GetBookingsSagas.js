import { call, put } from 'redux-saga/effects';
import BookingsActions from '../Redux/GetBookingsRedux';

export function * getBookingsList (api, action) {
  const { date } = action;
  const response = yield call(api.getBookingsList, date);

  if (response.ok) {
    yield put(BookingsActions.getBookingsSuccess(response.data))
  } else {
    yield put(BookingsActions.getBookingsFailure())
  }
}
