import { takeLatest, all } from 'redux-saga/effects'
import API from '../Services/Api'
import FixtureAPI from '../Services/FixtureApi'
import DebugConfig from '../Config/DebugConfig'

/* ------------- Types ------------- */

import { StartupTypes } from '../Redux/StartupRedux'
//import { GithubTypes } from '../Redux/GithubRedux'

import { LoginStepOneTypes } from '../Containers/Authentification/Redux/LoginStepOneRedux'
import { LoginTypes } from '../Containers/Authentification/Redux/LoginRedux'
import { SettingsTypes } from '../Redux/SettingsRedux'
import { SearchTypes } from '../Redux/SearchRedux'
import { ShopTypes } from '../Redux/ShopRedux'
import { ShopBarbersTypes } from '../Redux/ShopBarbersRedux'
import { ProfileTypes } from '../Redux/ProfileRedux'
import { UpdateProfileTypes } from '../Redux/UpdateProfileRedux'
import { AddBookingTypes } from '../Redux/AddBookingRedux'
import { BookingsTypes } from '../Redux/GetBookingsRedux'

/* ------------- Sagas ------------- */

import { startup } from './StartupSagas'
//import { getUserAvatar } from './GithubSagas'
import { updateLanguage } from './SettingsSagas'

import { setPhoneNumber } from '../Containers/Authentification/Sagas/LoginStepOneSagas'
import { getLoginVerifCode } from '../Containers/Authentification/Sagas/LoginSagas'
import { ressendPhoneCode } from '../Containers/Authentification/Sagas/LoginSagas'
import { getLogin } from '../Containers/Authentification/Sagas/LoginSagas'
import { createUser } from '../Containers/Authentification/Sagas/LoginSagas'
import { searchByShop } from './SearchSagas'
import { getShopDetails } from './ShopSagas'
import { getShopBarbers } from './ShopBarbersSagas'
import { getProfileDetails } from './ProfileSagas'
import { updateProfile } from './UpdateProfileSagas'
import { addBooking } from './AddBookingSagas'
import { getBookingsList } from './GetBookingsSagas'

/* ------------- API ------------- */

// The API we use is only used from Sagas, so we create it here and pass along
// to the sagas which need it.
const api = DebugConfig.useFixtures ? FixtureAPI : API.create()

/* ------------- Connect Types To Sagas ------------- */

export default function * root () {
  yield all([

    takeLatest(StartupTypes.STARTUP, startup),
    // some sagas only receive an action
    takeLatest(SettingsTypes.CHANGE_LANGUAGE, updateLanguage),
    
    // some sagas receive extra parameters in addition to an action
    //takeLatest(GithubTypes.USER_REQUEST, getUserAvatar, api),

    takeLatest(LoginStepOneTypes.LOGIN_STEP_ONE_REQUEST, setPhoneNumber, api),
    
    takeLatest(LoginTypes.LOGIN_VERIF_CODE_REQUEST, getLoginVerifCode, api),
    takeLatest(LoginTypes.LOGIN_SEND_CODE_REQUEST, ressendPhoneCode, api),
    takeLatest(LoginTypes.LOGIN_REQUEST, getLogin, api),
    takeLatest(LoginTypes.LOGIN_CREATE_REQUEST, createUser, api),
    takeLatest(SearchTypes.SEARCH_REQUEST, searchByShop, api),
    takeLatest(ShopTypes.GET_SHOP_REQUEST, getShopDetails, api),
    takeLatest(ShopBarbersTypes.GET_SHOP_BARBERS_REQUEST, getShopBarbers, api),
    takeLatest(ProfileTypes.GET_PROFILE_REQUEST, getProfileDetails, api),
    takeLatest(UpdateProfileTypes.UPDATE_PROFILE_REQUEST, updateProfile, api),
    takeLatest(AddBookingTypes.ADD_BOOKING_REQUEST, addBooking, api),
    takeLatest(BookingsTypes.GET_BOOKINGS_REQUEST, getBookingsList, api)
  ])
}
