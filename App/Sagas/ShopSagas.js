import { call, put } from 'redux-saga/effects';
import ShopActions from '../Redux/ShopRedux';

export function * getShopDetails (api, action) {
  const { shopId } = action;

  const response = yield call(api.getShopsDetails, shopId);

  if (response.ok) {
    yield put(ShopActions.getShopSuccess(response.data))
  } else {
    yield put(ShopActions.getShopFailure())
  }
}
