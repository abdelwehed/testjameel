import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  getProfileRequest: ['userId'],
  getProfileSuccess: ['payload'],
  getProfileFailure: null,
})

export const ProfileTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  userId: '',
  fetching: false,
  userInfos: null
})

/* ------------- Reducers ------------- */

// request the data from an api
export const request = (state, { userId }) => {
  console.log({ RESPONSE: userId})

  const newState = {
    ...state,
    fetching: true,
    userId,
    userInfos: null
  }
  return newState;
}

// successful api lookup
export const success = (state, action) => {
  const { payload } = action;

  const newState = {
    ...state,
    fetching: false,
    error: null,
    userInfos: payload
  }
  return newState;
}

// Something went wrong somewhere.
export const failure = state => {
  console.log('FAILURE')

  const newState = {
    ...state,
    fetching: false,
    error: true,
    userInfos: null
  }
  return newState;
}

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.GET_PROFILE_REQUEST]: request,
  [Types.GET_PROFILE_SUCCESS]: success,
  [Types.GET_PROFILE_FAILURE]: failure,
})
