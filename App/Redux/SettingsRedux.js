import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'
import DeviceInfo from 'react-native-device-info'

import I18n from '../assets/I18n'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  changeLanguage: ['language', 'isRTL']
})

export const SettingsTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  language: DeviceInfo.getDeviceLocale(), //I18n.locale.substr(0, 2) detect device language
  isRTL:false
})

/* ------------- Selectors ------------- */

export const SettingsSelectors = {
  getData: state => state.language
}

/* ------------- Reducers ------------- */

export const changeLanguage = (state, {language, isRTL}) => state.merge({
  language, isRTL
})


/* ------------- Hookup Reducers To Types ------------- */
export const reducer = createReducer(INITIAL_STATE, {
  [Types.CHANGE_LANGUAGE]: changeLanguage,
})