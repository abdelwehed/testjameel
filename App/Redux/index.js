import { combineReducers } from 'redux'
import { persistReducer } from 'redux-persist'
import configureStore from './CreateStore'
import rootSaga from '../Sagas/'
import ReduxPersist from '../Config/ReduxPersist'

/* ------------- Assemble The Reducers ------------- */
export const reducers = combineReducers({
  nav: require('./NavigationRedux').reducer,
  github: require('./GithubRedux').reducer,
  //github: require('./GithubRedux').reducer,

  loginStepOne: require('../Containers/Authentification/Redux/LoginStepOneRedux').reducer,
  login: require('../Containers/Authentification/Redux/LoginRedux').reducer,
  settings: require('./SettingsRedux').reducer,
  search: require('../Redux/SearchRedux').reducer,
  shopDetails: require('../Redux/ShopRedux').reducer,
  shopBarbers: require('../Redux/ShopBarbersRedux').reducer,
  profile: require('../Redux/ProfileRedux').reducer,
  profile: require('../Redux/ProfileRedux').reducer,
  updateProfile: require('../Redux/UpdateProfileRedux').reducer,
  addBooking: require('../Redux/AddBookingRedux').reducer,
  BookingsList: require('./GetBookingsRedux').reducer
})

export default () => {
  let finalReducers = reducers
  // If rehydration is on use persistReducer otherwise default combineReducers
  if (ReduxPersist.active) {
    const persistConfig = ReduxPersist.storeConfig
    finalReducers = persistReducer(persistConfig, reducers)
  }

  let { store, sagasManager, sagaMiddleware } = configureStore(finalReducers, rootSaga)

  if (module.hot) {
    module.hot.accept(() => {
      const nextRootReducer = require('./').reducers
      store.replaceReducer(nextRootReducer)

      const newYieldedSagas = require('../Sagas').default
      sagasManager.cancel()
      sagasManager.done.then(() => {
        sagasManager = sagaMiddleware.run(newYieldedSagas)
      })
    })
  }

  return store
}
