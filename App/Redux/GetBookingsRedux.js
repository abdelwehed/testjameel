import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  getBookingsRequest: ['date'],
  getBookingsSuccess: ['payload'],
  getBookingsFailure: null,
})

export const BookingsTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  date: '',
  fetching: false,
  bookings: null
})

/* ------------- Reducers ------------- */

// request the data from an api
export const request = (state, { date }) => {
  const newState = {
    ...state,
    fetching: true,
    date,
    bookings: null
  }
  return newState;
}

// successful api lookup
export const success = (state, action) => {
  const { payload } = action;
  const newState = {
    ...state,
    fetching: false,
    error: null,
    bookings: payload
  }
  return newState;
}

// Something went wrong somewhere.
export const failure = state => {
  const newState = {
    ...state,
    fetching: false,
    error: true,
    bookings: null
  }
  return newState;
}

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.GET_BOOKINGS_REQUEST]: request,
  [Types.GET_BOOKINGS_SUCCESS]: success,
  [Types.GET_BOOKINGS_FAILURE]: failure,
})
