import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  searchRequest: ['searchTerm'],
  searchSuccess: ['payload'],
  searchFailure: null,
})

export const SearchTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  searchTerm: '',
  searching: false,
  results: null
})

/* ------------- Reducers ------------- */

// request the data from an api
export const request = (state, { searchTerm }) => {
  const newState = {
    ...state,
    searching: true,
    searchTerm,
    results: null
  }
  return newState;
  // return state.merge({ searching: true, searchTerm, payload: null });
}

// successful api lookup
export const success = (state, action) => {
  const { payload } = action;
  const newState = {
    ...state,
    searching: false,
    error: null,
    results: payload
  }
  return newState;
}

// Something went wrong somewhere.
export const failure = state => {
  const newState = {
    ...state,
    searching: false,
    error: true,
    results: null
  }
  return newState;
}
  //state.merge({ searching: false, error: true, payload: null })

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.SEARCH_REQUEST]: request,
  [Types.SEARCH_SUCCESS]: success,
  [Types.SEARCH_FAILURE]: failure,
})
