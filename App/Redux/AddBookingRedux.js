import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  addBookingRequest: ['params'],
  addBookingSuccess: ['payload'],
  addBookingFailure: null,
})

export const AddBookingTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  params: {},
  fetching: false,
  wellSaved: null
})

/* ------------- Reducers ------------- */

// request the data from an api
export const request = (state, {params}) => {
  const newState = {
    ...state,
    fetching: true,
    params,
    wellSaved: null
  }
  return newState;
}

// successful api lookup
export const success = (state, action) => {
  const { payload } = action;
  const newState = {
    ...state,
    fetching: false,
    error: null,
    wellSaved: payload
  }
  return newState;
}

// Something went wrong somewhere.
export const failure = state => {
  const newState = {
    ...state,
    fetching: false,
    error: true,
    wellSaved: null
  }
  return newState;
}

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.ADD_BOOKING_REQUEST]: request,
  [Types.ADD_BOOKING_SUCCESS]: success,
  [Types.ADD_BOOKING_FAILURE]: failure,
})
