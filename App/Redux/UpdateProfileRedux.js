import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  updateProfileRequest: ['userId', 'params'],
  updateProfileSuccess: ['payload'],
  updateProfileFailure: null,
})

export const UpdateProfileTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  userId: '',
  params: {},
  fetching: false,
  userInfos: null
})

/* ------------- Reducers ------------- */

// request the data from an api
export const request = (state, { userId }, params) => {
  console.log({ REQUEST: userId, params})
  const newState = {
    ...state,
    fetching: true,
    userId,
    params,
    userInfos: null
  }
  return newState;
}

// successful api lookup
export const success = (state, action) => {
  const { payload } = action;
  const newState = {
    ...state,
    fetching: false,
    error: null,
    userInfos: payload
  }
  return newState;
}

// Something went wrong somewhere.
export const failure = state => {
  const newState = {
    ...state,
    fetching: false,
    error: true,
    userInfos: null
  }
  return newState;
}

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.UPDATE_PROFILE_REQUEST]: request,
  [Types.UPDATE_PROFILE_SUCCESS]: success,
  [Types.UPDATE_PROFILE_FAILURE]: failure,
})
