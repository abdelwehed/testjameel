import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  getShopBarbersRequest: ['shopId'],
  getShopBarbersSuccess: ['payload'],
  getShopBarbersFailure: null,
})

export const ShopBarbersTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  shopId: '',
  fetching: false,
  barbers: null
})

/* ------------- Reducers ------------- */

// request the data from an api
export const request = (state, { shopId }) => {
  const newState = {
    ...state,
    fetching: true,
    shopId,
    barbers: null
  }
  return newState;
}

// successful api lookup
export const success = (state, action) => {
  const { payload } = action;
  const newState = {
    ...state,
    fetching: false,
    error: null,
    barbers: payload
  }
  return newState;
}

// Something went wrong somewhere.
export const failure = state => {
  const newState = {
    ...state,
    fetching: false,
    error: true,
    barbers: null
  }
  return newState;
}

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.GET_SHOP_BARBERS_REQUEST]: request,
  [Types.GET_SHOP_BARBERS_SUCCESS]: success,
  [Types.GET_SHOP_BARBERS_FAILURE]: failure,
})
