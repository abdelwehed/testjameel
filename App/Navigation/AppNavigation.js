import { put, select } from 'redux-saga/effects'
import React from 'react'
import  I18n  from 'react-native-i18n'
import { StackNavigator, TabNavigator, SwitchNavigator} from 'react-navigation'
import LoginScreen from '../Containers/Authentification/LoginScreen'
import LoginStepOneScreen from '../Containers/Authentification/LoginStepOneScreen'
import NewBookingScreen from '../Containers/Account/NewBookingScreen'
import HomeScreen from '../Containers/Account/HomeScreen'
import StoreSearchList from '../Containers/Account/StoreSearchList'
import HairCutShopScreen from '../Containers/Account/HiarcutShopScreen'
import SettingsScreen from '../Containers/Account/SettingsScreen'
import AccountScreen from '../Containers/Account/AccountScreen'
import NotificationsScreen from '../Containers/Account/Notifications'
import LoadingScreen from '../Containers/LoadingScreen'
import BookingScreen from '../Containers/Account/BookingScreen'
import BookingsListScreen from '../Containers/Account/BookingsList'
import BookingFirstStepScreen from '../Containers/Account/BookingFirstStep'
import BookingSecondStepScreen from '../Containers/Account/BookingSecondStep'
import BookingThirdStepScreen from '../Containers/Account/BookingThirdStep'
import ProfileScreen from '../Containers/Account/ProfileScreen'
import PointsScreen from '../Containers/Account/PointsScreen'
import FavoriteScreen from '../Containers/Account/FavoriteScreen'
import { Image } from 'react-native'
import { Images } from '../Themes'
import styles from './Styles/NavigationStyles'
import BookinDetailsScreen from '../Containers/Account/BookingDetail'

export const selectLanguage = state => state.settings.language // get the language from the settings reducer
const language = select(selectLanguage)
const translate = key => I18n.t(`screens.${key}`);

const tabOptions = {
  tabBarOptions: {
      activeTintColor:'#0f93cd',
      inactiveTintColor:'black',
      tabBarPosition: 'top',
      style:{
          borderTopWidth:1,
          borderTopColor:'#0f93cd',
          height:70,
          paddingBottom: 10,
          paddingTop: 10
      },
      tabBarSelectedItemStyle: {
          borderBottomWidth: 1,
          borderBottomColor: 'red',
      }
  },
  tabBarPosition: 'bottom',
  lazy: true  /*Hey look at here Lazy is true*/
}

const HomeStack = StackNavigator({
  ProfileScreen: { 
    screen: HomeScreen,
    navigationOptions: {
      title: translate('home.title'),
    }
  }
});

const BookingStack = StackNavigator({
  ProfileScreen: { 
    screen: BookingScreen,
    navigationOptions: {
      title: translate('bookings.title'),
    }
  },
  NewBookingScreen: { screen: NewBookingScreen },
  StoreSearchList: { screen: StoreSearchList },
  HairCutShop: { screen: HairCutShopScreen },
  BookingStepOne: { screen: BookingFirstStepScreen },
  BookingStepTwo: { screen: BookingSecondStepScreen },
  BookingStepThree: { screen: BookingThirdStepScreen },
  BookingsListScreen: { screen: BookingsListScreen },
  BookinDetailsScreen: { screen: BookinDetailsScreen },
});

const PointsStack = StackNavigator({
  ProfileScreen: { 
    screen: PointsScreen,
    navigationOptions: {
      title: translate('points.title'),
    }
  }
});

const FavoritesStack = StackNavigator({
  ProfileScreen: { 
    screen: FavoriteScreen,
    navigationOptions: {
      title: translate('favs.title'),
    }
  }
});

const SettingsStack = StackNavigator({
  SettingsScreen: { 
    screen: SettingsScreen,
    navigationOptions: {
      title: translate('settings.title'),
    }
  },
  AccountScreen: { 
    screen: AccountScreen,
    navigationOptions: {
      title: I18n.t('account'),
    }
  },
  NotificationsScreen: { 
    screen: NotificationsScreen,
    navigationOptions: {
      title: I18n.t('notifications'),
    }
  },
  ProfileScreen: { 
    screen: ProfileScreen,
    navigationOptions: {
      title: I18n.t('account'),
    }
  },
});

const Tabs = TabNavigator({
    Profile: {
      screen: HomeStack,
      navigationOptions: {
        title: translate('home.title'),
        tabBarIcon: ({ tintColor }) => <Image source={Images.home} style={{ resizeMode: 'contain', height: 25, width: 25}} />
      }
    },
    Booking: {
      screen: BookingStack,
      navigationOptions: {
        title: translate('bookings.title'),
        tabBarIcon: ({ tintColor }) => <Image source={{uri:Images.iconBooking}} style={{ height: 25, width: 25}} />
      }
    },
    Points: {
      screen: PointsStack,
      navigationOptions: {
        title: translate('points.title'),
        tabBarIcon: ({ tintColor }) => <Image source={{uri:Images.iconPoints}} style={{ height: 25, width: 25}} />
      }
    },
    Favorites: {
      screen: FavoritesStack,
      navigationOptions: {
        title: translate('favs.title'),
        tabBarIcon: ({ tintColor }) => <Image source={{uri:Images.iconFavorite}} style={{ height: 25, width: 25}} />
      }
    },
    Settings: {
      screen: SettingsStack,
      navigationOptions: {
        title: translate('settings.title'),
        tabBarIcon: ({ tintColor }) => <Image source={{uri:Images.iconSetting}} style={{ height: 25, width: 25}} />
      }
    }
  },
  tabOptions
)



// Manifest of possible screens
const AppStack = StackNavigator({
  MainScreen: {
    screen: Tabs
  },
}, {
  headerMode: 'none',
  initialRouteName: 'MainScreen',
  navigationOptions: {
    headerStyle: styles.header
  }
})

const AuthStack = StackNavigator({ 
  LoginStepOneScreen: { screen: LoginStepOneScreen },
  LoginScreen: { screen: LoginScreen }
},
{
  headerMode: 'none',
  initialRouteName: 'LoginStepOneScreen',
})



export default  SwitchNavigator(
  {
    AuthLoading: LoadingScreen,
    App: AppStack,
    Auth: AuthStack,
  },
  {
    initialRouteName: 'AuthLoading',
  }
)
