import React, { Component } from 'react'
import { TouchableOpacity, Image, Keyboard } from 'react-native'
import { Text } from 'react-native-elements'
import { Icon, Input, Item, Form, View } from "native-base";
import Autocomplete from "native-base-autocomplete";
import { connect } from 'react-redux'
import { Images } from '../Themes'
import SearchActions from '../Redux/SearchRedux'
// Styles
import styles from './Styles/autocompleteSearch';
class AutocompleteSearch extends Component {
  state = {
    searchText: "",
    hideSuggestions: true
  }

  onSearchChange = text => {
    this.setState({ searchText: text, hideSuggestions: text.length < 1 });
    if (text.length >= 2) this.props.searchShops(text.toLowerCase());
  };

  render() {
    const { placeholder, iconRight, iconLeft, searchResults, navigation: { navigate } } = this.props;
    const { searchText, hideSuggestions } = this.state;

    return (
      <View style={{flex: 0.2}}>
      <Autocomplete
        data={searchResults !== null ? searchResults : []}
        hideResults={searchText.length < 2 || hideSuggestions}
        inputContainerStyle={styles.inputContainer}
        renderTextInput={() => (
          <Form style={styles.form}>
            <Item regular style={styles.item}>
              <Image source={{uri: iconLeft ? iconLeft : Images.iconBooking}} style={styles.leftIcon} />
              <Input
                placeholder={placeholder}
                onChangeText={text => this.onSearchChange(text)}
                onSubmitEditing={() =>
                  this.setState({ hideSuggestions: true })
                }
                returnKeyType={"search"}
                value={this.state.searchQuery}
              />
              <Icon active name={iconRight} />
            </Item>
          </Form>
        )}
        renderItem={data => (
            <TouchableOpacity
            onPress={() => {
              this.setState({ hideSuggestions: true });
              navigate('HairCutShop', { shop: data });
              Keyboard.dismiss();
            }}
              style={{ padding: 10 }}
            >
              <Text>{data.name}</Text>
            </TouchableOpacity>
          )
        }
        />
        </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    searchResults: state.search.results
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    searchShops: (searchTerm) => dispatch(SearchActions.searchRequest(searchTerm)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AutocompleteSearch);