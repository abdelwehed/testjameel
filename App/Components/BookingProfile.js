import React, { Component } from 'react'
import { Text, Image, View } from 'react-native'
import { Images, Metrics } from '../Themes'
// components
import IconTextItem from '../Components/screensComponents/iconTextItem'
// styles
import styles from './Styles/BookingProfile'

export default class BookingProfile extends Component {
  render () {
    return (
        <View style={styles.container}>
          <Image source={{ uri: 'https://archzine.fr/wp-content/uploads/2017/10/coiffure-homme-long-dessus-de%CC%81grade%CC%81-homme-tondeuse.jpg'}}
          style={styles.image} resizeMode='contain' />

          <Text style={styles.name}>Matt K.</Text>

          <IconTextItem
          icon={Images.star}
          iconStyle={{width: 15, height: 15}}
          textStyle={{fontSize: 15}}
          displayText />
        </View>
    )
  }
}