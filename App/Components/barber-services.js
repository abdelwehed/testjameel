import React, { Component } from 'react';
import {View, StyleSheet, Text, FlatList, TouchableOpacity, AsyncStorage } from 'react-native';
import { Icon } from 'react-native-elements';
// styles
import barberStyles from './Styles/barber-services';

export default class BarberServices extends Component {
  state = {
    selectedIndex: null
  }

  componentDidMount = () => {
    const { data, navigation } = this.props;
    const firstBookingObject = navigation.getParam('firstBookingObject');

    AsyncStorage.getItem('booking').then((value) => {
      const savedBooking = JSON.parse(value);
      if(savedBooking!==null) {
        const savedStoreId = parseInt(savedBooking.store.slice(-1), 10);

        if(!firstBookingObject || (firstBookingObject && savedStoreId === firstBookingObject.store.id)) {
          const foundData = data.filter(el => el.name === savedBooking.service);
          const index = data.indexOf(foundData[0]);
          this.setState({ selectedIndex: index });
          
          this.props.onSelect(data[index]);
        } else if(firstBookingObject.store.id && savedStoreId !== firstBookingObject.store.id) {
          AsyncStorage.removeItem('booking');
        }
      }
    }).done();
  }

  selecService = (item, index) => {
    const { selectedIndex } = this.state;
    const selectedItem = item.item;

    if(index === selectedIndex) {
      this.setState({ selectedIndex: null });
      this.props.onSelect(null);
    } else {
      this.setState({ selectedIndex: index });
      this.props.onSelect(selectedItem);
    }
  }

  renderItem = ( item ) => {
    const { showEditButtons } = this.props;
    const { selectedIndex } = this.state;
    const { item: { label, price, time } } = item;
    const index = item.index;
    
    return <TouchableOpacity
    style={[barberStyles.itemContainer, { backgroundColor: selectedIndex === index ? '#0f93cd' : null }]}
    onPress={() => this.selecService(item, index)}
    >
      <View>
        <Text style={{ fontWeight: 'bold' }}>{label}</Text>
        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <Text style={barberStyles.text}>{price}</Text>
          <Text style={barberStyles.text}>{time}</Text>
        </View>
      </View>

      {showEditButtons && <TouchableOpacity style={barberStyles.updateButton}>
        <Icon name='create' color="#0f93cd" size={15} />
      </TouchableOpacity>}
    </TouchableOpacity>
    }

    render () {
      const { data, title, maxHeight } = this.props;
      const { selectedIndex } = this.state;
      return (
        <View style={barberStyles.container}>
        {title && <Text style={barberStyles.title}>{title}</Text>}
        <FlatList
        extraData={selectedIndex}
      contentContainerStyle={styles.list}
      data={data}
      renderItem={ (i) => {
        return (<View style={[barberStyles.item, { maxHeight: maxHeight ? maxHeight : 70 }]}>
          {this.renderItem(i)}
          </View>)
      }}
      />
      </View>);
    }
}

const styles = StyleSheet.create({
  list: {
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    flexWrap: 'wrap',
  }
});