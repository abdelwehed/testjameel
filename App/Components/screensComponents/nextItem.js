import React, { Component } from 'react'
import { View, Image } from 'react-native'
import { Icon, Text, Button} from 'react-native-elements'
import { Images, Metrics } from '../../Themes'

class NextItem extends Component {
  render () {
    return (
        <View style={{width: Metrics.screenWidth, flexDirection: 'row', justifyContent: 'center'}}>
          <Text style={{fontWeight: 'bold', fontSize: 30}}>Next Booking</Text>
          <Image 
          source={{uri: Images.chevronRight }}
          style={{ width: 30, height: 30, marginLeft: 10 }}
          />
        </View>
    )
  }
}

export default NextItem;
