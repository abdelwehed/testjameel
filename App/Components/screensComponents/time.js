import React, { Component } from 'react'
import { View, Image } from 'react-native'
import { Icon, Text, Button} from 'react-native-elements'
import { Images, Metrics } from '../../Themes'

class Time extends Component {
  render () {
    return (
        <View style={{width: Metrics.screenWidth, flexDirection: 'row',
         justifyContent: 'flex-start', alignItems: 'center', paddingLeft: 40 }}>
          <Image 
          source={{uri: Images.watch }}
          style={{ width: 40, height: 40, marginRight: 15 }}
          />
          <Text style={{fontSize: 40, fontWeight: 'bold', color: '#0f93cd'}}>16:50</Text>
        </View>
    )
  }
}

export default Time;
