import React, { Component } from 'react'
import { View, Image } from 'react-native'
import { Icon, Text, Button} from 'react-native-elements'
import { Images, Metrics } from '../../Themes'

class NextItem extends Component {
  render () {
    const { displayText, iconStyle, icon, textStyle, text } = this.props;
    return (
        <View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
          <Image 
          source={{ resizeMode: 'contain', uri: icon ? icon : 'https://facebook.github.io/react-native/docs/assets/favicon.png'}}
          style={[iconStyle, {marginRight: 10 }]}
          />
          {displayText === true && <Text style={textStyle}>{text ? text : 'jameel barber'}</Text>}
        </View>
    )
  }
}

export default NextItem;
