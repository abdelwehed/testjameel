import React, { Component } from 'react'
import { View, ScrollView, Text, TouchableOpacity } from 'react-native'
import { Thumbnail } from 'native-base';
import { Metrics } from '../../Themes'

class HorizentalList extends Component {
  state = {
    selectedIndex: null
  }

  selectBarber = (index) => {
    const { data } = this.props;
    const { selectedIndex } = this.state;

    if(index === selectedIndex) {
      this.setState({ selectedIndex: null });
      this.props.onSelect(null);
    } else {
      this.setState({ selectedIndex: index });
      this.props.onSelect(data[index]);
    }
  }

  renderList = (data) => {
    const { selectedIndex } = this.state;

    return(
      <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
        {data.map((barber, i) => {
          const barberImage = barber.photo !== null ? `http://51.68.122.76:8080/media/${barber.photo.contentUrl}` :
          'https://mbalit.co.uk/wp-content/uploads/2018/02/blank-head.png';
          return(
            <TouchableOpacity
              key={i}
              onPress={() => this.selectBarber(i)}
              style={{flexDirection: 'column',
              alignItems: 'center',
              margin: 7,
              backgroundColor: selectedIndex === i ? '#0f93cd' : null
            }}
            >
              <Thumbnail 
              source={{uri: barberImage}}
              />
              <Text>{barber.username}</Text>
            </TouchableOpacity>
          )
        })
        }
      </View>
    )
  }

  render () {
    const { title, data } = this.props;
    console.log({ shopBarbers: data });

    return (
        <View style={{ width: Metrics.screenWidth - 80, justifyContent: 'center' }}>
          <Text style={{ fontSize: 20, fontWeight: 'bold' }}>{title}</Text>
          <ScrollView horizontal showsHorizontalScrollIndicator={false}>
            {this.renderList(data)}
          </ScrollView>
        </View>
    )
  }
}

export default HorizentalList;
