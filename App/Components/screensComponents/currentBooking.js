import React, { Component } from 'react'
import { View, Image } from 'react-native'
import { Divider, Icon, Text, Button} from 'react-native-elements'
import { Images, Metrics } from '../../Themes'
// components
import Spacer from '../Spacer';
import IconTextItem from './iconTextItem';
import Time from './time';
import NextItem from './nextItem';

class CurrentBooking extends Component {
  render () {
    return (
      <View style={{ flex: 1, flexDirection: 'column' }}>

        <NextItem />
        
        <Spacer size={30} />
        
        <View style={{ width: Metrics.screenWidth, flexDirection: 'row', paddingLeft: 40 }}>
          <IconTextItem
          icon={Images.store}
          iconStyle={{width: 12, height: 12}}
          displayText />
          
          <IconTextItem
          icon={Images.cut}
          iconStyle={{width: 12, height: 12}}
          displayText />
        </View>

        <Spacer size={10} />

        <Time />

        <Spacer size={10} />

        <View style={{ width: Metrics.screenWidth, paddingLeft: 40 }}>
          <IconTextItem
          icon={Images.calendar}
          iconStyle={{width: 12, height: 12}}
          displayText />

          <Spacer size={10} />
          <IconTextItem
          icon={Images.position}
          iconStyle={{width: 12, height: 18}}
          displayText />
          </View>
      </View>
    )
  }
}

export default CurrentBooking;
