import React from 'react';
import { View } from 'react-native';

const Spacer = ({ size }) => (
  <View
    style={{
      left: 0,
      right: 0,
      height: 1,
      marginTop: size - 1,
    }}
  />
);

export default Spacer;
