import React, { Component } from 'react';
import { AsyncStorage, Text } from 'react-native';
import { CalendarList } from 'react-native-calendars';

const disabledDates = [
  '2018-10-06', '2018-10-08', '2018-10-10', '2018-10-12'
];

export default class StepTwo extends Component {
  state = {
    selectedDate: null,
     activeDay: null
  };

    componentDidMount = () => {
      AsyncStorage.getItem('booking').then((value) => {
        const savedBooking = JSON.parse(value);

        if(savedBooking!==null) {
          this.setState({ selectedDate: savedBooking.startAt.slice(0, 10) });
          this.props.onSelect(savedBooking.startAt.slice(0, 10));
        }
      }).done();
    }

    componentWillMount = () => {
      const today = new Date();
      const year = today.getFullYear();
      let month = today.getMonth() + 1;
      let day = today.getDay();

      if(month <= 9) {
        month = `0${month}`;
      }
      if(day <= 9) {
        day = `0${day}`
      }

      let newDaysObject = {};

      disabledDates.forEach((day) => {
        newDaysObject = {
          ...newDaysObject,
          [day]: {
            disabled: true,
            disableTouchEvent: true
          }
        };
      });

      this.setState({ activeDay: `${year}-${month}-${day}`, datesOff: newDaysObject});
    }

    selectDate = (day) => {
      this.setState({ selectedDate: day.dateString });
      this.props.onSelect(day.dateString);
    }

    render () {
      return (
          <CalendarList
            // Enable horizontal scrolling, default = false
            horizontal={true}
            // Enable paging on horizontal, default = false
            pagingEnabled={true}
            // Initially visible month. Default = Date()
            current={this.state.activeDay}
            // Minimum date that can be selected, dates before minDate will be grayed out. Default = undefined
            minDate={this.state.activeDay}
            // Maximum date that can be selected, dates after maxDate will be grayed out. Default = undefined
            maxDate={'2100-12-31'}
            // Handler which gets executed on day press. Default = undefined
            onDayPress={(day) => this.selectDate(day)}
            // Handler which gets executed on day long press. Default = undefined
            // onDayLongPress={(day) => this.selectDate(day)}
            // Month format in calendar title. Formatting values: http://arshaw.com/xdate/#Formatting
            monthFormat={'MMMM yyyy'}
            // Handler which gets executed when visible month changes in calendar. Default = undefined
            onMonthChange={(month) => {console.log('month changed', month)}}
            // Hide month navigation arrows. Default = false
            hideArrows={true}
            // Replace default arrows with custom ones (direction can be 'left' or 'right')
            renderArrow={(direction) => (<Text>></Text>)}
            // Do not show days of other months in month page. Default = false
            hideExtraDays={false}
            // If hideArrows=false and hideExtraDays=false do not switch month when tapping on greyed out
            // day from another month that is visible in calendar page. Default = false
            disableMonthChange={true}
            // If firstDay=1 week starts from Monday. Note that dayNames and dayNamesShort should still start from Sunday.
            firstDay={1}
            // Hide day names. Default = false
            hideDayNames={false}
            // Show week numbers to the left. Default = false
            onPressArrowLeft={substractMonth => substractMonth()}
            // Handler which gets executed when press arrow icon left. It receive a callback can go next month
            onPressArrowRight={addMonth => addMonth()}
            markingType={'custom'}
            markedDates={{...this.state.datesOff, [this.state.selectedDate]: {
              customStyles: {
                container: {
                  backgroundColor: '#0f93cd',
                },
                text: {
                  color: 'white',
                  fontWeight: 'bold'
                },
              },
            },}}
              // theme
              theme={{
                'stylesheet.calendar.header': {
                  dayHeader: {
                    color: '#0f93cd',
                  }
                },
                'stylesheet.calendar.day': {
                  height: 10,
                  dayHeader: {
                    height: 10,
                  }
               }
              }}
          />
      );
    }
}