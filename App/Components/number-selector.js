import React, { Component } from 'react'
import { View, Text, ScrollView, TouchableOpacity } from 'react-native'

const selectedItemStyle = {
  borderTopWidth: 2,
  borderColor: '#0f93cd',
  borderBottomWidth: 2,
};

const slectedTextStyle = {
  fontWeight: 'bold',
  opacity: 1
};

const normalTextStyle = {
  fontWeight: 'bold',
  opacity: 0.5
};

export default class BookingThirdStep extends Component {
  state = {
    activeIndex: 0,
  }

  onScrollEndDrag = (e) => {
    const itemIndex = Math.round(e.nativeEvent.contentOffset.y / 30);
    this.setState({ activeIndex: itemIndex });
  }

  selectNumber = (number) => {
    this.props.onSelect(number);
  }

  render () {
    const { title, data } = this.props;
    return (
          <View style={{ height: 140, alignItems: 'center' }}>
            <Text style={{ fontSize: 15, color: 'gray', marginBottom: 20 }}>{title}</Text>
            <ScrollView
            onScrollEndDrag={(e) => this.onScrollEndDrag(e)}
            showsVerticalScrollIndicator={false}
            >
            {data.map((el, i) => {
              return (<TouchableOpacity
                key={i}
                onPress={() => this.selectNumber(el)}
                style={this.state.activeIndex === i && selectedItemStyle}
                >
                <Text style={[this.state.activeIndex === i ? slectedTextStyle : normalTextStyle, { color: 'black', padding: 10 }]}>
                  {el}
                </Text>
                </TouchableOpacity>)
            })}
            </ScrollView>
          </View>
    )
  }
}