import React, { Component } from 'react';
import { Text, View } from 'react-native';
import { Button } from 'native-base';
import { Images, Metrics } from '../Themes';
// components
import IconTextItem from '../Components/screensComponents/iconTextItem';
import Spacer from '../Components/Spacer';
// styles
import styles from './Styles/BarberInfo';

export default class BarberInfos extends Component {
  render () {
    return (
      <View style={styles.container}>
        <View style={styles.subContainer}>
          <IconTextItem
          displayText
          iconStyle={{ width: 15, height: 15 }}
          icon={Images.store}
          textStyle
          />

          <Spacer size={10} />

          <IconTextItem
          displayText
          iconStyle={{ width: 12, height: 18 }}
          icon={Images.position}
          textStyle
          text="North, London Street"
          />

          <Spacer size={10} />

          <IconTextItem
          displayText
          iconStyle={{ width: 15, height: 15 }}
          icon={Images.star}
          textStyle
          text="154 View"
          />
        </View>

        <Button
        style={styles.button}
        transparent
        info
        onPress={this.handleMakeBookingClick}
      >
       <Text style={styles.buttonText}>Edit</Text>
      </Button>
      </View>
    )
  }
}