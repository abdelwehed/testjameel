import React, { Component } from 'react'
import { Text, Image, View, ActivityIndicator } from 'react-native'
import { Button, Input } from 'native-base'
import { connect } from 'react-redux'
import DialogInput from 'react-native-dialog-input'
import UpsateProfileActions from '../Redux/UpdateProfileRedux';
// styles
import styles from './Styles/BarberProfile'

class BarberProfile extends Component {
  state = {
    toggleDialog: false,
    variableToChange: null,
    emptyInput: null,
    text: null
  }

  openDialog = variable => {
    this.setState({ toggleDialog: true, variableToChange: variable });
  }

  sendInput = (text, id) => {
    const { variableToChange } = this.state;
    let param = {};
    if(variableToChange === 'fullname') param = { fullname: text }
    if(variableToChange === 'phone') param = { phone: text }
    if(variableToChange === 'email') param = { email: text }

    if(text!=='') {
      this.props.updateProfile(id, param);
      this.setState({ toggleDialog: false });
    }else {
      this.setState({ emptyInput: `Please enter a valid ${this.state.variableToChange}` });
    }
  }

  render () {
    const { id, name, phone, email, updatedProfileInfos, updateProfileFetching, photo } = this.props;
    const { toggleDialog, variableToChange, emptyInput } = this.state;

    const displayName = updatedProfileInfos !== null ? updatedProfileInfos.fullname : name;
    const displayPhone = updatedProfileInfos !== null ? updatedProfileInfos.phone : phone;
    const displayEmail = updatedProfileInfos !== null ? updatedProfileInfos.email : email;
    const profilePhoto = photo!== null ? `http://51.68.122.76:8080/media/${photo.contentUrl}` :
    'https://mbalit.co.uk/wp-content/uploads/2018/02/blank-head.png';

    if(updateProfileFetching) return <ActivityIndicator />

    return (
        <View style={styles.container}>
          <Image source={{ uri: profilePhoto }}
          style={styles.imageProfile} resizeMode='contain' />

          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <Text style={styles.name}>{displayName}</Text>
            <Button
              style={styles.button}
              transparent
              info
              onPress={() => this.openDialog('fullname')}
            >
              <Text style={styles.buttonText}>Edit</Text>
            </Button>
          </View>

          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <Text style={styles.tel}>{displayPhone}</Text>
{/*             <Button
              style={styles.button}
              transparent
              info
              onPress={() => this.openDialog('phone')}
            >
              <Text style={styles.buttonText}>Edit</Text>
</Button> */}
          </View>

          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
          <Text style={styles.tel}>{displayEmail}</Text>
            <Button
              style={styles.button}
              transparent
              info
              onPress={() => this.openDialog('email')}
            >
              <Text style={styles.buttonText}>Edit</Text>
            </Button>
          </View>

          <DialogInput isDialogVisible={toggleDialog}
              title={"Update Profile"}
              modalStyle={{backgroundColor: 'black', opacity: 0.8}}
              dialogStyle={{backgroundColor: '#fff'}}
              message={emptyInput ? emptyInput : `Updating ${variableToChange}`}
              hintInput ={"Update"}
              submitInput={ (inputText) => this.sendInput(inputText, id) }
              closeDialog={ () => { this.setState({ toggleDialog: false, emptyInput: null }) } }>
          </DialogInput>
        </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    updatedProfileInfos: state.updateProfile.userInfos,
    updateProfileFetching: state.updateProfile.fetching
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    updateProfile: (userId, params) => dispatch(UpsateProfileActions.updateProfileRequest(userId, params)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(BarberProfile)