import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'space-around',
    flexDirection: 'column',
    padding: 20
  },
  imageProfile: {
    height: 80,
    width: 80,
    borderRadius: 40
  },
  button: {
    alignSelf: 'center'
  },
  buttonText: {
    color: "#0f93cd"
  },
  name: {
    fontWeight: 'bold',
    fontSize: 20
  },
  tel: {
    fontSize: 18 
  }
})