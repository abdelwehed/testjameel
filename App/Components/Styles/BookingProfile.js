
import { StyleSheet } from 'react-native'
import { Metrics } from '../../Themes/'

export default StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1.4
  },
  image: {
    height: 100,
    width: 100,
    borderRadius: 50
  },
  name: {
    fontWeight: 'bold',
    fontSize: 22
  }
})