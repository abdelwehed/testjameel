import { StyleSheet } from 'react-native';
import { Metrics } from '../../Themes';

export default StyleSheet.create({
  container: {
    width: Metrics.screenWidth - 60,
    padding: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  subContainer: {
    alignItems: 'flex-start',
    flexDirection: 'column'
  },
  button: {
    alignSelf: 'center'
  },
  buttonText: {
    color: "#0f93cd"
  }
})