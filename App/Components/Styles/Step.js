import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  container: {
    alignSelf: 'center',
    fontWeight: 'bold',
    fontSize: 15
  }
})