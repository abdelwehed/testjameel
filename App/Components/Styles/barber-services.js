import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  container: {
    flex: 1
  },
  title: {
    fontWeight: 'bold',
    fontSize: 18,
    paddingLeft: 30
  },
  itemContainer: {
    flexDirection: 'row'
  },
  item: {
    flex: 1,
    margin: 5,
    minWidth: 100,
    height: 50,
    backgroundColor: 'transparent',
    alignItems: 'center',
    justifyContent: 'center'
  },
  updateButton: {
    marginLeft: 5,
    backgroundColor: 'transparent',
    alignItems: 'center'
  },
  text: {
    fontSize: 10,
    fontWeight: 'bold'
  }
})