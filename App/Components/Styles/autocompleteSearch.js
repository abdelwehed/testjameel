import { StyleSheet } from 'react-native'
import { Metrics } from '../../Themes/'

export default StyleSheet.create({
  inputContainer: {
    borderBottomWidth: 0,
    backgroundColor: 'transparent' 
  },
  form: {
    backgroundColor: "#fff" 
  },
  leftIcon: {
    resizeMode: 'contain',
    height: 20,
    width: 20,
    marginLeft: 10
  },
  item: {
    width: Metrics.screenWidth - 60,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: '#0f93cd'
  }
})