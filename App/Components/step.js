import React from 'react';
import { Text } from 'react-native';
import I18n from '../assets/I18n';
import styles from './Styles/Step';

const translate = key => I18n.t(`components.booking.${key}`);

const Step = ({step}) => ( <Text style={styles.container}>{translate('step')} {step} / 3</Text> );

export default Step;