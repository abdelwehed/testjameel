// a library to wrap and simplify api calls
import apisauce from 'apisauce'
import AppConfig from '../Config/AppConfig'

// our "constructor"
const create = (baseURL = AppConfig.apiUrl) => {
  // ------
  // STEP 1
  // ------
  //
  // Create and configure an apisauce-based api object.
  //
  const api = apisauce.create({
    // base URL is read from the "constructor"
    baseURL,
    // here are some default headers
    headers: {
      'Cache-Control': 'no-cache',
      'content-type': 'application/json'
    },
    // 10 second timeout...
    timeout: 10000
  })

  // ------
  // STEP 2
  // ------
  //
  // Define some functions that call the api.  The goal is to provide
  // a thin wrapper of the api layer providing nicer feeling functions
  // rather than "get", "post" and friends.
  //
  // I generally don't like wrapping the output at this level because
  // sometimes specific actions need to be take on `403` or `401`, etc.
  //
  // Since we can't hide from that, we embrace it by getting out of the
  // way at this level.
  //
  const getRoot = () => api.get('')
  const getRate = () => api.get('rate_limit')
  const getUser = (username) => api.get('search/users', {q: username})
  
  const setAuthToken = (userAuth) => api.setHeader('Authorization', 'Bearer ' + userAuth)

  const setphonenumber = ({phonenumber}) => api.post('phone_numbers/send_verification_codes', {number: phonenumber })
  const getloginverifcode = ({phonenumber}) => api.get('/phone_numbers/get_verification_code/'+phonenumber)
  const sendphonecode = ({phonenumber}) => api.get('/phone_numbers/send_verification_code/'+phonenumber)
  const getlogin = ({username,phonenumber}) => api.post('login_check', {username: username, password: phonenumber })
  const register = (user) => api.post('/users', user)
  const getShopsList = (searchTerm) => api.get('/stores/?name=' + searchTerm)
  const getShopsDetails = (shopId) => api.get('/stores/' + shopId)
  const getShopsBarbers = (shopId) => api.get('/users?id=' + shopId)
  const getProfile = (userId) => api.get('/users/' + userId)
  const updateProfile = (userId, params) => api.put('/users/' + userId, params)
  const addBooking = (params) => api.post('/bookings' ,params)
  //const getCurrentBookings = (date) => api.get('bookings?startAt[after]=' + date)
  const getBookingsList = (date) => api.get('bookings?startAt' + date)

  // ------
  // STEP 3
  // ------
  //
  // Return back a collection of functions that we would consider our
  // interface.  Most of the time it'll be just the list of all the
  // methods in step 2.
  //
  // Notice we're not returning back the `api` created in step 1?  That's
  // because it is scoped privately.  This is one way to create truly
  // private scoped goodies in JavaScript.
  //
  return {
    // a list of the API functions from step 2
    getRoot,
    getRate,
    getUser,
    setAuthToken,
    setphonenumber,
    getloginverifcode,
    sendphonecode,
    getlogin,
    register,
    getShopsList,
    getShopsDetails,
    getShopsBarbers,
    getProfile,
    updateProfile,
    addBooking,
    //getCurrentBookings,
    getBookingsList
  }
}

// let's return back our create method as the default.
export default {
  create
}
