import Fonts from './Fonts'
import Metrics from './Metrics'
import Colors from './Colors'

// This file is for a reusable grouping of Theme items.
// Similar to an XML fragment layout in Android

const ApplicationStyles = {
  screen: {
    mainContainer: {
      flex: 1,
      backgroundColor: Colors.transparent
    },
    backgroundImage: {
      position: 'absolute',
      top: 0,
      left: 0,
      bottom: 0,
      right: 0
    },
    container: {
      flex: 1,
      paddingTop: Metrics.baseMargin,
      backgroundColor: Colors.transparent
    },
    section: {
      margin: Metrics.section,
      padding: Metrics.baseMargin
    },
    sectionText: {
      ...Fonts.style.normal,
      paddingVertical: Metrics.doubleBaseMargin,
      color: Colors.clear,
      marginVertical: Metrics.smallMargin,
      textAlign: 'center'
    },
    exampleText: {
      ...Fonts.style.normal,
      //paddingVertical: Metrics.doubleBaseMargin,
      color: Colors.steel,
      //marginVertical: Metrics.smallMargin,
      textAlign: 'center'
    },
    subtitle: {
      color: Colors.snow,
      padding: Metrics.smallMargin,
      marginBottom: Metrics.smallMargin,
      marginHorizontal: Metrics.smallMargin
    },
    titleText: {
      ...Fonts.style.h2,
      fontSize: 14,
      color: Colors.text
    },////////////////////////
    inputStyle: {
      paddingLeft: 5,
      marginBottom: 10,
      borderRadius: 10,
      borderWidth: 1,
      borderBottomColor: Colors.inputText,
      borderLeftColor: Colors.inputText,
      borderRightColor: Colors.inputText,
      borderTopColor: Colors.inputText,
      backgroundColor: Colors.whites  
    },
    buttonStyle: {
      backgroundColor: Colors.inputText,
      borderRadius: 10,
      marginBottom: 10
    },
    divider: {
      height: 3,
      color: Colors.inputText,
      marginVertical: Metrics.smallMargin,
    },
    loginNow: {
      ...Fonts.style.bold,
      paddingVertical: Metrics.doubleBaseMargin,
      marginVertical: Metrics.smallMargin,
      textAlign: 'center',
      color: Colors.clear,
      fontSize: 30,
    },
    txtTitle: {
      paddingVertical: Metrics.smallMargin,
      marginVertical: Metrics.smallMargin,
      textAlign: 'center'
    },
    erorText: {
      ...Fonts.style.normal,
      paddingVertical: Metrics.doubleBaseMargin,
      color: Colors.red,
      marginVertical: Metrics.smallMargin,
      textAlign: 'center'
    },
    /////////////////////////
  },
  darkLabelContainer: {
    padding: Metrics.smallMargin,
    paddingBottom: Metrics.doubleBaseMargin,
    borderBottomColor: Colors.border,
    borderBottomWidth: 1,
    marginBottom: Metrics.baseMargin
  },
  darkLabel: {
    fontFamily: Fonts.type.bold,
    color: Colors.snow
  },
  groupContainer: {
    margin: Metrics.smallMargin,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center'
  },
  sectionTitle: {
    ...Fonts.style.h4,
    color: Colors.coal,
    backgroundColor: Colors.ricePaper,
    padding: Metrics.smallMargin,
    marginTop: Metrics.smallMargin,
    marginHorizontal: Metrics.baseMargin,
    borderWidth: 1,
    borderColor: Colors.ember,
    alignItems: 'center',
    textAlign: 'center'
  }
}

export default ApplicationStyles
