// Simple React Native specific changes

import '../assets/I18n/I18n'

export default {
  apiUrl: 'http://51.68.122.76:8080/', //'http://localhost:8080/'
  
  // font scaling override - RN default is on
  allowTextFontScaling: true
}
