import React, { Component } from 'react'
import { ScrollView, Text, KeyboardAvoidingView, AsyncStorage } from 'react-native'
import { connect } from 'react-redux'
import I18n from '../assets/I18n'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import styles from './Styles/LoadingScreenStyle'

class LoadingScreen extends Component {

  constructor(props) {
    super(props);
    
    this.state = {
      userToken: this.props.userToken,
    }
   this._bootstrapAsync()
  }

  // Fetch the token from storage then navigate to our appropriate place
  _bootstrapAsync = async () => {
    const userToken = await AsyncStorage.getItem('userToken')
    this.props.navigation.navigate(userToken ? 'App' : 'Auth')
  }

  componentWillReceiveProps(nextProps){
    //const userToken =  nextProps.userToken
  }

  render () {
    return (
      <ScrollView>
        <KeyboardAvoidingView behavior='position'>
          <Text></Text>
        </KeyboardAvoidingView>
      </ScrollView>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    userToken:state.login.payload
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(LoadingScreen)
