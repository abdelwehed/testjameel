/* ***********************************************************
* A short word on how to use this automagically generated file.
* We're often asked in the ignite gitter channel how to connect
* to a to a third party api, so we thought we'd demonstrate - but
* you should know you can use sagas for other flow control too.
*
* Other points:
*  - You'll need to add this saga to sagas/index.js
*  - This template uses the api declared in sagas/index.js, so
*    you'll need to define a constant in that file.
*************************************************************/

import { call, put, select } from 'redux-saga/effects'
import LoginActions from '../Redux/LoginRedux'
import { NavigationActions } from 'react-navigation';
import { ActivityIndicator, AsyncStorage } from 'react-native';
// import { LoginSelectors } from '../Redux/LoginRedux'

export const stepOneState = state => state.loginStepOne;

export function * getLoginVerifCode (api, action) {
  const stepOne = yield select(stepOneState)
  const { username, phonenumber} = stepOne.data

  const { data } = action
  // get current data from Store
  // const currentData = yield select(LoginVerifCodeSelectors.getData)
  // make the call to the api
  const response = yield call(api.getloginverifcode, data)
  // success?
  if (response.ok) {
    yield put(LoginActions.loginVerifCodeSuccess(response.data))

    if(response.data.verificationCode === action.data.phonecode){
      console.log('VerifCode ok')
      yield put(LoginActions.loginRequest({username, phonenumber}))
    } else {
      console.log('Error VerifCode')
    }
  } else {
    yield put(LoginActions.loginVerifCodeFailure())
  }
}

export function * getLogin (api, action) {
  const { data } = action
  // get current data from Store
  // const currentData = yield select(LoginSelectors.getData)
  // make the call to the api
  const response = yield call(api.getlogin, data)
  // success?
  if (response.ok) {
    // You might need to change the response here - do this with a 'transform',
    // located in ../Transforms/. Otherwise, just pass the data back from the api.
    AsyncStorage.setItem('userToken', response.data.token);
    yield put(LoginActions.loginSuccess(response.data))

    yield put(NavigationActions.navigate({ routeName: 'MainScreen' }));
  } else {
    console.log('data to create user', data)
    const {username, phonenumber} = data
    //if phone code verified and user not exist
    yield put(LoginActions.loginCreateRequest({username, phonenumber}))
    //yield put(LoginActions.loginFailure())

  }
}


export function * ressendPhoneCode (api, action) {
  const { data } = action
  // get current data from Store
  // const currentData = yield select(LoginVerifCodeSelectors.getData)
  // make the call to the api
  const response = yield call(api.sendphonecode, data)

  // success?
  if (response.ok) {
    //response.data.action = action.type
    // You might need to change the response here - do this with a 'transform',
    // located in ../Transforms/. Otherwise, just pass the data back from the api.
    yield put(LoginActions.loginSendCodeSuccess(response.data))
  } else {
    yield put(LoginActions.loginSendCodeFailure())
  }
}


export function * createUser (api, action) {
  const { data } = action
  user = {
    "email": data.username+"@jameel.jb",
    "fullname": data.username,
    "username": data.username,
    "phone": data.phonenumber,
    "plainPassword": data.phonenumber,
    "enabled": true,
    "roles": ["ROLE_BARBER"]
  }
  // get current data from Store
  // const currentData = yield select(LoginVerifCodeSelectors.getData)
  // make the call to the api
  const response = yield call(api.register, user)

  //console.log('Created user action <<<<<<<<>', action)

  // success?
  if (response.ok) {
    //console.log('Succes created user <<<<<<<<>', response)
    // You might need to change the response here - do this with a 'transform',
    // located in ../Transforms/. Otherwise, just pass the data back from the api.
    yield put(LoginActions.loginCreateSuccess(response.data))
    yield put(LoginActions.loginRequest(data))
  } else {
    yield put(LoginActions.loginCreateFailure())
  }
}
