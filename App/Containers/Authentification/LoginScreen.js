import React, { Component } from 'react'
import { ScrollView, Text, KeyboardAvoidingView, Image, View } from 'react-native'
import { Button, FormLabel, FormInput, FormValidationMessage } from 'react-native-elements'
import { connect } from 'react-redux'

import I18n from '../../assets/I18n'
import { Images } from '../../Themes'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

import LoginActions from './Redux/LoginRedux'

// Styles
import styles from './Styles/LoginScreenStyle'

class LoginScreen extends Component {

  constructor (props) {
    super(props)
    this.state = {
      phonecode: '',
      username: props.username,
      phonenumber: props.phonenumber,
      msgError: '',
      countDown: '',
      disabledRessend: true
    }
    this._startCountdown()
  }

  handleChangePhoneCode = (text) => {
    this.setState({ phonecode: text })
  }

  handleRessendCode = () => {
    const { phonenumber } = this.state
    this.props.attemptoRessendCode(phonenumber)
    this._startCountdown()
  }

  handlePressGo = () => {
    const { phonenumber, phonecode } = this.state

    if(phonecode == '' ){
      this.setState ({ msgError: "Please enter a valid Code"})
    }else{
      this.props.attemptoVerifPhone(phonenumber, phonecode)
    }
  }

  _startCountdown = () => {
    let counter = 60
    let interval = setInterval( () => {
      this.setState({ disabledRessend: true })
      this.setState({ countDown: counter })
      counter--

      if(counter < 0 ){
        clearInterval(interval)
        this.setState({ disabledRessend: false })
        this.setState({ countDown: I18n.t('resend code') })
      }
    }, 1000)
  }


  componentWillReceiveProps(nextProps){
    //console.log('nextProps', nextProps)
  }

  render () {

    //const { disabledRessend } = this.state

    return (
      <View style={styles.mainContainer}>
        <Image source={Images.background} style={styles.backgroundImage} resizeMode='stretch' />
        <ScrollView style={styles.container}>
         <KeyboardAvoidingView behavior='position'> behavior='position'>
            <View style={styles.centered}>
              <Image style={styles.logo} source={{ uri: Images.logo }} />
            </View>

            <View style={styles.section} >
              <Text style={styles.sectionText}>
                { I18n.t('start now') }
              </Text>

              <FormInput 
                ref='phonecode'
                placeholder={ I18n.t('verification code') }
                autoCapitalize='none'
                keyboardType={'numeric'}
                maxLength={6}
                minLength={6}
                autoCorrect={false}
                containerStyle={styles.inputStyle} 
                underlineColorAndroid='transparent'
                onChangeText={this.handleChangePhoneCode}
                onSubmitEditing={this.handlePressGo}
                />

              <Text style={styles.erorText}>
                {this.state.msgError}
              </Text>

              <Button 
                title={ I18n.t('go') }
                buttonStyle={styles.buttonStyle} 
                onPress={this.handlePressGo} />
              <Button 
                ref='ressendCode'
                title={ this.state.countDown }
                disabled={ this.state.disabledRessend }
                buttonStyle={styles.buttonStyle}
                onPress={this.handleRessendCode} />
            </View>

          </KeyboardAvoidingView>
        </ScrollView>
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    phonenumber: state.loginStepOne.data.phonenumber,
    username: state.loginStepOne.data.username,
    //
    fetching: state.login.fetching,
    payload: state.login.payload,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    attemptoVerifPhone: (phonenumber, phonecode) => dispatch(LoginActions.loginVerifCodeRequest({phonenumber, phonecode})),
    attemptoRessendCode: (phonenumber) => dispatch(LoginActions.loginSendCodeRequest({phonenumber})),
    //attemptoLogin: (username, phonenumber) => dispatch(LoginActions.loginRequest({username, phonenumber}))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen)