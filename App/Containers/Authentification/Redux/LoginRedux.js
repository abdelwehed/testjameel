import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'
import { NavigationActions } from 'react-navigation'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  loginVerifCodeRequest: ['data'],
  loginVerifCodeSuccess: ['payload'],
  loginVerifCodeFailure: null,
  //
  loginSendCodeRequest: ['data'],
  loginSendCodeSuccess: ['payload'],
  loginSendCodeFailure: null,
  //
  loginRequest: ['data'],
  loginSuccess: ['payload'],
  loginFailure: null,
  //
  //
  loginCreateRequest: ['data'],
  loginCreateSuccess: ['payload'],
  LoginCreateFailure: null,
  //
  logout: null,
})

export const LoginTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  data: null,
  fetching: null,
  payload: null,
  error: null
})

/* ------------- Selectors ------------- */

export const LoginSelectors = {
  getData: state => state.data
}

/* ------------- Reducers ------------- */

// request the data from an api
export const request = (state, { data }) =>
  state.merge({ fetching: true, data, payload: null })

// successful api lookup
export const success = (state, action) => {
  const { payload } = action
  return state.merge({ fetching: false, error: null, payload })
}

// Something went wrong somewhere.
export const failure = state =>
  state.merge({ fetching: false, error: true, payload: null })


  /********** we've logged out  ****************/
export const logout = (state) => {
  //NavigationActions.navigate({ routeName: 'LoginStepOneScreen' })
  return INITIAL_STATE //state.merge({ payload: null })
}

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.LOGIN_VERIF_CODE_REQUEST]: request,
  [Types.LOGIN_VERIF_CODE_SUCCESS]: success,
  [Types.LOGIN_VERIF_CODE_FAILURE]: failure,

  [Types.LOGIN_REQUEST]: request,
  [Types.LOGIN_SUCCESS]: success,
  [Types.LOGIN_FAILURE]: failure,
  
  [Types.LOGIN_SEND_CODE_REQUEST]: request,
  [Types.LOGIN_SEND_CODE_SUCCESS]: success,
  [Types.LOGIN_SEND_CODE_FAILURE]: failure,

  [Types.LOGIN_CREATE_REQUEST]: request,
  [Types.LOGIN_CREATE_SUCCESS]: success,
  [Types.LOGIN_CREATE_FAILURE]: failure,

  [Types.LOGOUT]: logout,
})
