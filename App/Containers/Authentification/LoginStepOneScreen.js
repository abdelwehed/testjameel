import React, { Component } from 'react'
import { ScrollView, Text, KeyboardAvoidingView, Image, View } from 'react-native'
import { Button, FormLabel, FormInput, FormValidationMessage } from 'react-native-elements'
import { connect } from 'react-redux'

import I18n from '../../assets/I18n'

import { Images } from '../../Themes'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

import LoginStepOneActions from './Redux/LoginStepOneRedux'

// Styles
import styles from './Styles/LoginStepOneScreenStyle'

class LoginStepOneScreen extends Component {

  constructor (props) {
    super(props)
    this.state = {
      username: '',
      phonenumber: '',
      msgError: ''
    }
  }

  handleChangeUserName = (text) => {
    this.setState({ username: text })
  }

  handleChangePhone = (text) => {
    this.setState({ phonenumber: text })
  }

  handlePressNext = () => {
    const { username, phonenumber } = this.state
    if(username == '' || phonenumber == ''){
        this.setState ({ msgError: "Please enter a valid name or phone"})
    }else{
      this.props.attemptRegisterPhone(phonenumber, username)
    }
  }


  componentWillReceiveProps(nextProps){

  }


  render () {
    const { username, phonenumber } = this.state
    const { fetching } = this.props
    const editable = true //!fetching
    return (
      <View style={styles.mainContainer}>
        <Image source={Images.background} style={styles.backgroundImage} resizeMode='stretch' />
        <ScrollView style={styles.container}>
         <KeyboardAvoidingView behavior='position'> behavior='position'>
            <View style={styles.centered}>
              <Image style={styles.logo} source={{ uri: Images.logo }} />
            </View>

            <View style={styles.section} >
              <Text style={styles.sectionText}>
                { I18n.t('start now') }
              </Text>

              <FormInput
                ref='username'
                value={username}
                editable={editable}
                containerStyle={styles.inputStyle}
                inputStyle={{ textAlign: 'left', width: '90%' }}
                keyboardType='default'
                returnKeyType='next'
                autoCapitalize='none'
                autoCorrect={false}
                onChangeText={this.handleChangeUserName}
                underlineColorAndroid='transparent'
                onSubmitEditing={() => this.refs.phonenumber.focus()}
                placeholder= { I18n.t('name') }/>

              <Text style={ styles.exampleText}> 
                { I18n.t('phone example') }
              </Text>

              <FormInput
                ref='phonenumber'
                value={phonenumber}
                autoCapitalize='none'
                autoCorrect={false}
                maxLength={14}
                keyboardType={'numeric'}
                containerStyle={styles.inputStyle}
                inputStyle={{ textAlign: 'left', width: '90%' }}
                onChangeText={this.handleChangePhone}
                underlineColorAndroid='transparent'
                onSubmitEditing={this.handlePressNext}
                placeholder= { I18n.t('phone number') }/>

              <Text style={styles.erorText}>
                {this.state.msgError}
              </Text>

              <Button
                title={ I18n.t('next') }
                buttonStyle={styles.buttonStyle}
                onPress={this.handlePressNext}
                />


          </View>
          </KeyboardAvoidingView>
        </ScrollView>
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    fetching:state.loginStepOne.fetching,
    payload:state.loginStepOne.payload,
    //language: state.settings.language
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    attemptRegisterPhone: ( phonenumber, username) => dispatch(LoginStepOneActions.loginStepOneRequest({ phonenumber, username})),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginStepOneScreen)
