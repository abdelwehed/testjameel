import React, { Component } from 'react'
import { TouchableOpacity, Text, Image, View, AsyncStorage } from 'react-native'
import I18n from '../../assets/I18n'
import { Images } from '../../Themes'
// Styles
import styles from './Styles/BookingScreenStyle'

const translate = key => I18n.t(`components.booking.${key}`);

const BookingButton = ({ onPress, text }) => (
  <TouchableOpacity onPress={onPress} style={styles.button}>
    <Text style={styles.buttonText}>{text}</Text>
  </TouchableOpacity>
);
export default class AccountScreen extends Component {
  handleNewBookingClick = () => {
    const { navigation: { navigate } } = this.props;
    navigate('NewBookingScreen');
  }

  handleBookingsListClick = (state) => {
    const { navigation: { navigate } } = this.props;
    navigate('BookingsListScreen', {state: state});
  }

  render () {
    const buttons = [
      {name: translate('makeNew'), onPress: this.handleNewBookingClick},
      {name: translate('current'), onPress: () => this.handleBookingsListClick('current')},
    ];

    return (
      <View style={styles.mainContainer}>
        <Image source={Images.background} style={styles.backgroundImage} resizeMode='stretch' />
        {buttons.map((item, i) => {
          return  <BookingButton onPress={item.onPress} text={item.name} />
        })}
      </View>
    )
  }
}