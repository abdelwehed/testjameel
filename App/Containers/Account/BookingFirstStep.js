import React, { Component } from 'react'
import { Image, View, Text, AsyncStorage } from 'react-native'
import { Button } from 'react-native-elements'
import I18n from '../../assets/I18n'
import { Images } from '../../Themes'
// Styles
import styles from './Styles/BookingSteps'
// components
import Step from '../../Components/step';
import BookingProfile from '../../Components/BookingProfile';
import BarberServices from '../../Components/barber-services';

const data = [{key: 'a', name: '/services/1', label: 'Service 1', price: '$30', time: '10 min'}, {key: 'b', name: '/services/2', label: 'Service 2', price: '$30', time: '10 min'},
      {key: 'c', name: '/services/3', label: 'Service 3', price: '$30', time: '10 min'},{key: 'd', name: '/services/4', label: 'Service 4', price: '$30', time: '10 min'},
      {key: 'e', name: '/services/5', label: 'Service 5', price: '$30', time: '10 min'},{key: 'f', name: '/services/6', label: 'Service 6', price: '$30', time: '10 min'},
      {key: 'g', name: '/services/7', label: 'Service 7', price: '$30', time: '10 min'}, {key: 'h', name: '/services/8', label: 'Service 8', price: '$30', time: '10 min'},
      {key: 'i', name: '/services/9', label: 'Service 9', price: '$30', time: '10 min'}];

const translate = key => I18n.t(`components.booking.${key}`);
export default class BookingFirstStep extends Component {
  state = {
    service: null
  };

  handleNextClick = () => {
    const { service } = this.state;
    const { navigation: { navigate }, navigation } = this.props;

    AsyncStorage.getItem('booking').then((value) => {
      const savedBooking = JSON.parse(value);
      let firstStepBookingObject = navigation.getParam('firstBookingObject');
      if(savedBooking!==null) {
        firstStepBookingObject = {
          store: savedBooking.store,
          barber: savedBooking.barber
        }
      }
      const secondBookingObject = {
        ...firstStepBookingObject,
        service: service
      }
      service !==null && navigate('BookingStepTwo', { secondBookingObject: secondBookingObject });
    }).done();
  }

  render () {
    const { service } = this.state;
    const { navigation } = this.props;
    
    return (
      <View style={styles.mainContainer}>
        <Image source={Images.background} style={styles.backgroundImage} resizeMode='stretch' />
        <BookingProfile />

        <View style={styles.body}>
          <Text style={styles.title}>{translate('service-selection')}</Text>
          <BarberServices
          navigation={navigation}
          data={data}
          style={{ flex: 2 }}
          onSelect={ (selectedService) => { this.setState({ service: selectedService }) } }
          />
        </View>

        <View style={styles.bottom}>
          <Button
              disabled={service === null}
              title={ translate('next') }
              buttonStyle={styles.buttonStyle}
              onPress={this.handleNextClick}
          />
          <Step step='1' />
        </View>
      </View>
    )
  }
}