import React, { Component } from 'react'
import { View, Image, ActivityIndicator } from 'react-native'
import { Button} from 'react-native-elements'
import { connect } from 'react-redux'
import geolib from 'geolib';
import ShopActions from '../../Redux/ShopRedux';
import ShopBarbersActions from '../../Redux/ShopBarbersRedux';

import I18n from '../../assets/I18n'
import { Images } from '../../Themes'
// Styles
import styles from './Styles/HaircutShopScreen'
//components
import IconTextItem from '../../Components/screensComponents/iconTextItem';
import Spacer from '../../Components/Spacer';
import HorizentalList from '../../Components/screensComponents/horizentalList';

const translate = key => I18n.t(`components.booking.${key}`);
const screenTranslate = key => I18n.t(`screens.shop.${key}`);

class HairCutShopScreen extends Component {
  state = {
    BarberSelected: null
  }

  handleMakeBookingClick = () => {
    const { navigation: { navigate }, navigation } = this.props;
    const { BarberSelected } = this.state;
    const shop = navigation.getParam('shop');
    const firstBookingObject = {
      store: shop,
      barber: BarberSelected
    }
    navigate('BookingStepOne', { firstBookingObject: firstBookingObject });
  }
  
  getCurrentPosition = () => {
    navigator.geolocation.getCurrentPosition(
      (position) => {
        return {
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
        }
      },
      (error) => {},
      { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 },
    );
  }

  componentDidMount = () => {
    const { navigation } = this.props;
    const shop = navigation.getParam('shop');

    this.props.getShopDetails(shop.id);
    this.props.getShopBarbers(shop.id);
  }

  getShopDistance = (shop) => {
    const { latitude: shopLatitude, longitude: shopLongitude } = shop;
    const currentPosition = this.getCurrentPosition();
    const distance = geolib.getDistance(
      {
        latitude: currentPosition ? currentPosition.latitude : 111111,
        longitude: currentPosition ? currentPosition.longitude : 111111
      },
      {
        latitude: shopLatitude,
        longitude: shopLongitude
      }
    );

    return `${distance} km away`;
  }


  render () {
    const { BarberSelected } = this.state;
    const { shopDetails, detailsFetching, barbersFetching, shopBarbers } = this.props;
    const displayBarbers = shopBarbers && shopBarbers.length > 0;

    const shopImage = (shopDetails && shopDetails.image) ? 
    `http://51.68.122.76:8080/media/${shopDetails.image.contentUrl}` : 
    'https://static4.pagesjaunes.fr/media/ugc/l_homme_02737500_235231408';

    if(detailsFetching && barbersFetching) return <ActivityIndicator />

    return (
      <View style={styles.container}>
      <Image source={Images.background} style={styles.backgroundImage} resizeMode='stretch' />
        <View style={{flex: 3}}>
          <Image
          style={styles.image} 
          source={{uri: shopImage
          }}
          />
        </View>

        <View style={styles.details}>
          <IconTextItem
          icon={Images.store}
          iconStyle={{width: 20, height: 20}}
          textStyle={{fontSize: 24, fontWeight: 'bold'}}
          text={shopDetails && shopDetails.name}
          displayText />

          <Spacer size={10} />

          <IconTextItem
          icon={Images.position}
          iconStyle={{width: 12, height: 18}}
          textStyle={{fontSize: 12}}
          text={shopDetails && this.getShopDistance(shopDetails)}
          displayText />

          <Spacer size={10} />

          <IconTextItem
          icon={Images.stars}
          iconStyle={{width: 12, height: 12}}
          textStyle={{fontSize: 12}}
          displayText />
        </View>

        {displayBarbers && <View style={{flex: 2}}>
          <HorizentalList
          onSelect={ (isSelected) => { this.setState({ BarberSelected: isSelected }) } }
          title={screenTranslate('barbers')}
          data={shopBarbers}
          />
        </View>}
        
        <View style={styles.button}>
          <Button
              disabled={BarberSelected===null}
              title={ translate('make') }
              buttonStyle={styles.buttonStyle}
              onPress={() => this.handleMakeBookingClick()}
          />
        </View>
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    shopDetails: state.shopDetails.details,
    shopBarbers: state.shopBarbers.barbers,
    detailsFetching: state.shopDetails.fetching,
    barbersFetching: state.shopBarbers.fetching
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getShopDetails: (shopId) => dispatch(ShopActions.getShopRequest(shopId)),
    getShopBarbers: (shopId) => dispatch(ShopBarbersActions.getShopBarbersRequest(shopId)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(HairCutShopScreen)
