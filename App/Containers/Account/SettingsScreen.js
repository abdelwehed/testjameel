import React from 'react'
import { Alert,View, Text, FlatList, Image, TouchableOpacity } from 'react-native'
import { connect } from 'react-redux'

// More info here: https://facebook.github.io/react-native/docs/flatlist.html

import SettingsActions from '../../Redux/SettingsRedux'
import I18n from '../../assets/I18n'

// Styles
import { Images } from '../../Themes'
import styles from './Styles/SettingsStyle'


class Settings extends React.PureComponent {

  constructor (props) {
    super(props)
  }

  switchLanguage= () => {
    if( this.props.language == 'ar'){
      this.props.attemptSwitchLanguage('en')
    }else{
      this.props.attemptSwitchLanguage('ar')
    }
  }

  goToAccount = () => { this.props.navigation.navigate({ routeName: 'AccountScreen' }) }
  goToNtifications = () => { this.props.navigation.navigate({ routeName: 'NotificationsScreen' }) }
  goToInvits = () => { this.props.navigation.navigate({ routeName: 'ProfileScreen' }) }
  goToShare = () => { this.props.navigation.navigate({ routeName: 'AccountScreen' }) }
  goToReport = () => { this.props.navigation.navigate({ routeName: 'AccountScreen' }) }
  goToCall = () => { this.props.navigation.navigate({ routeName: 'AccountScreen' }) }
  goToAbout = () => { this.props.navigation.navigate({ routeName: 'AccountScreen' }) }
  goToPrivacy = () => { this.props.navigation.navigate({ routeName: 'AccountScreen' }) }

  /* ***********************************************************
  * STEP 1
  * This is an array of objects with the properties you desire
  * Usually this should come from Redux mapStateToProps
  *************************************************************/
  state = {
    dataObjects: [
        {title: I18n.t('account'), icon: Images.iconAccount, action: this.goToAccount },
        {title: I18n.t('notifications'), icon: Images.iconNotification, action: this.goToNtifications },
        {title: I18n.t('invits freinds'), icon: Images.iconInvitFreinds, action: this.goToInvits },
        {title: I18n.t('share account'), icon: Images.iconShare, action: this.goToShare },
        {title: I18n.t('report'), icon: Images.iconReport, action: this.goToReport },
        {title: I18n.t('call us'), icon: Images.iconContact, action: this.goToCall },
        {title: I18n.t('about jameel'), icon: Images.iconAbout, action: this.goToAbout },
        {title: I18n.t('privacy policy'), icon: Images.iconPrivacy, action: this.goToPrivacy },
    ]
  }

  /* ***********************************************************
  * STEP 2
  * `renderRow` function. How each cell/row should be rendered
  * It's our best practice to place a single component here:
  *
  * e.g.
    return <MyCustomCell title={item.title} description={item.description} />
  *************************************************************/
  renderRow ({item}) {
    return (
      <View style={styles.row}>
        <TouchableOpacity onPress={ item.action } >
          <Image 
            style={styles.iconStyle} 
            source={{ uri: item.icon }} 
          />
        </TouchableOpacity>
        <Text style={styles.boldLabel}>{item.title}</Text>
      </View>
    )
  }

  /* ***********************************************************
  * STEP 3
  * Consider the configurations we've set below.  Customize them
  * to your liking!  Each with some friendly advice.
  *************************************************************/
  // Render a header?
  renderHeader = () =>
    <Text style={[styles.label, styles.sectionHeader]}>  </Text>

  // Render a footer?
  renderFooter = () =>
    <Text style={[styles.label, styles.sectionHeader]}> </Text>

  // Show this when data is empty
  renderEmpty = () =>
    <Text style={styles.label}> - Nothing to See Here - </Text>

  renderSeparator = () =>
    <Text style={styles.label}>  </Text>

  // The default function if no Key is provided is index
  // an identifiable key is important if you plan on
  // item reordering.  Otherwise index is fine
  keyExtractor = (item, index) => index

  // How many items should be kept im memory as we scroll?
  oneScreensWorth = 10

  // extraData is for anything that is not indicated in data
  // for instance, if you kept "favorites" in `this.state.favs`
  // pass that in, so changes in favorites will cause a re-render
  // and your renderItem will have access to change depending on state
  // e.g. `extraData`={this.state.favs}

  // Optimize your list if the height of each item can be calculated
  // by supplying a constant height, there is no need to measure each
  // item after it renders.  This can save significant time for lists
  // of a size 100+
  // e.g. itemLayout={(data, index) => (
  //   {length: ITEM_HEIGHT, offset: ITEM_HEIGHT * index, index}
  // )}

  render () {
    return (
      <View style={styles.mainContainer}>
        <Image source={Images.background} style={styles.backgroundImage} resizeMode='stretch' />
          <FlatList style={{ padding: 10 }}
            contentContainerStyle={styles.listContent}
            contentContainerStyle={{ flex: 1 }}
            data={this.state.dataObjects}
            renderItem={this.renderRow}
            numColumns={2}
            keyExtractor={this.keyExtractor}
            initialNumToRender={this.oneScreensWorth}
            ListHeaderComponent={this.renderHeader}
            ListFooterComponent={this.renderFooter}
            ListEmptyComponent={this.renderEmpty}
            ItemSeparatorComponent={this.renderSeparator}
          />
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    // ...redux state to props here
    language: state.settings.language,
    isRTL: false
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    attemptSwitchLanguage: ( language, isRTL) => dispatch(SettingsActions.changeLanguage(language, isRTL)   )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Settings)
