import React, { Component } from 'react';
import { Image, View, Text } from 'react-native';
import { Button } from 'react-native-elements';
import I18n from '../../assets/I18n';
import { Images } from '../../Themes';
// Styles
import styles from './Styles/BookingSteps';
// components
import Step from '../../Components/step';
import BookingProfile from '../../Components/BookingProfile';
import BookingCalendar from '../../Components/booking-calendar';

const translate = key => I18n.t(`components.booking.${key}`);

export default class BookingSecondStep extends Component {
  state = {
    bookingDate: null
  };

  handleNextClick = () => {
    const { bookingDate } = this.state;
    const { navigation: { navigate }, navigation } = this.props;
    const secondBookingObject = navigation.getParam('secondBookingObject');

    const shouldNavigateToNextStep = secondBookingObject && bookingDate;
    const thirdBookingObject = {
      ...secondBookingObject,
      startAt: bookingDate
    }
    shouldNavigateToNextStep && navigate('BookingStepThree', { thirdBookingObject: thirdBookingObject });
  }

  render () {
    const { bookingDate } = this.state;
    
    return (
      <View style={styles.mainContainer}>
        <Image source={Images.background} style={styles.backgroundImage} resizeMode='stretch' />
        <BookingProfile />

        <View style={styles.body}>
          <Text style={styles.title}>{translate('date-selection')}</Text>
          <BookingCalendar
          onSelect={ (selectedDate) => { this.setState({ bookingDate: selectedDate }) } }
          />
        </View>

        <View style={styles.bottom}>
          <Button
              disabled={bookingDate === null}
              title={ translate('next') }
              buttonStyle={styles.buttonStyle}
              onPress={this.handleNextClick}
          />
          <Step step='2' />
        </View>
      </View>
    )
  }
}