import React, { Component } from 'react'
import { ScrollView, Text, Image, View } from 'react-native'
import { Images } from '../../Themes'

// Styles
import styles from './Styles/PointsScreenStyle'

export default class AccountScreen extends Component {
  render () {
    return (
      <View style={styles.mainContainer}>
        <Image source={Images.background} style={styles.backgroundImage} resizeMode='stretch' />
        <ScrollView style={styles.container}>
          <View style={styles.centered}>
            <Image style={styles.logo} source={{ uri: Images.logo }} />
          </View>

          <View style={styles.section} >
            <Image source={Images.ready} />
            <Text style={styles.sectionText}>
              Points details
            </Text>
          </View>

        </ScrollView>
      </View>
    )
  }
}
