import React, { Component } from 'react'
import { ScrollView, Image, View, TouchableOpacity } from 'react-native'
import { List, ListItem, Body } from 'native-base'
import { Images } from '../../Themes'
// Styles
import styles from './Styles/FavoriteScreenStyle'
// components
import IconTextItem from '../../Components/screensComponents/iconTextItem';
import Spacer from '../../Components/Spacer'

const data = [
  '', '', '', '', '', '', '', ''
];
export default class AccountScreen extends Component {
  renderList = () => {
    return(
      <View>
     {data.map((el, i) => {
      return (
       <List key={i}>
       <ListItem style={styles.listItem} itemHeader first>
       <TouchableOpacity onPress={this.handleShopClick} style={styles.item} >
           <View style={styles.avatarContainer}>
              <Image 
              source={{uri: Images.star}}
              style={styles.avatar}
              />
           </View>

           <Body style={styles.body}>
             <View style={styles.bodyLeft}>
                <IconTextItem
                icon={Images.store}
                iconStyle={{width: 15, height: 15}}
                displayText
                el={el} />

                <Spacer size={2} />

                <IconTextItem
                icon={Images.cut}
                iconStyle={styles.image}
                displayText />
             </View>

             <Spacer size={15} />

             <View style={styles.bodyRight}>
              <IconTextItem
              icon={Images.position}
              iconStyle={{width: 12, height: 18}}
              displayText />

              <Image 
              source={{uri: Images.chevronRight}}
              style={styles.image}
              />
             </View>
           </Body>
         </TouchableOpacity>

       </ListItem>
     </List>
      )
    })}
    </View>
   );
  }

  render () {
    return (
      <View style={styles.mainContainer}>
        <Image source={Images.background} style={styles.backgroundImage} resizeMode='stretch' />
        <ScrollView
        showsVerticalScrollIndicator={false}
        style={styles.container}>
        {this.renderList()}
        </ScrollView>
      </View>
    )
  }
}
