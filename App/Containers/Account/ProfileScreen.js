import React, { Component } from 'react'
import { ScrollView, View,Image, KeyboardAvoidingView, SectionList, FlatList, AsyncStorage } from 'react-native'
import { connect } from 'react-redux'

import { Avatar, Icon, Text, Button} from 'react-native-elements'

import I18n from '../../assets/I18n'

import { Images } from '../../Themes'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'
import LoginActions from '../Authentification/Redux/LoginRedux'

// Styles
import styles from './Styles/ProfileScreenStyle'

class ProfileScreen extends Component {

  constructor (props) {
    super(props)
    this.state = {
      username: props.username,
      phonenumber: props.phonenumber,
    }
  }

  handleLogout = async () => {
    try {
      await AsyncStorage.removeItem('userToken');
    } catch (error) {
      console.log('errorrrrrr',error)
    }
    this.props.navigation.navigate({ routeName: 'LoginStepOneScreen' })
    //this.props.logout()
  }

  render () {
    return (
      <View style={styles.mainContainer}>
        <Image source={Images.background} style={styles.backgroundImage} resizeMode='stretch' />
        <ScrollView style={styles.container}>
            <View style={{flex: 1, flexDirection: 'row', margin: 20}}>
              <Text> { I18n.t('start now', {locale: "en"}) } </Text>
              <Avatar
                rounded
                height={100}
                //icon={{name: 'user'}}
                onPress={() => console.log("Works!")}
                activeOpacity={0.7}
              />
              <View  style={{flex: 1, flexDirection: 'column', padding:25 }}>
                <Text style={{ fontSize: 20, fontWeight: 'bold' }}> { this.state.username }</Text>
                <Text style={{ fontSize: 16 }}> { this.state.phonenumber } </Text>
              </View>
            </View>

            <View style={{flex: 1, flexDirection: 'row', margin: 20}} >
              <FlatList
                data={[
                  {key: 'Jameel Barber', icon:'star'},
                  {key: 'Nrth Street ...', icon:'star'},
                  {key: '100 Reviews ', icon:'star'}
                ]}
                renderItem={({item}) => 
                  <View style={{flex: 1, flexDirection: 'row'}}>
                    <Icon name={item.icon} style={{ padding: 5}}/>
                    <Text style={{height: 30, padding: 5}}>{item.key} </Text>
                  </View>
                }
              />
            </View>

            <View style={{flex: 1, flexDirection: 'row', marginLeft:20}}>
              <View style={{flex: 1, height: 60 }} > <Text> Service 1 </Text> </View>
              <View style={{flex: 1, height: 60 }} >  <Text> Service 2 </Text> </View>
              <View style={{flex: 1, height: 60 }} >  <Text> Service 3 </Text> </View>
            </View>
            <View style={{flex: 1, flexDirection: 'row', marginLeft:20}}>
              <View style={{flex: 1, height: 60 }} > <Text> Service 4 </Text> </View>
              <View style={{flex: 1, height: 60 }} > <Text> Service 5 </Text> </View>
              <View style={{flex: 1, height: 60 }} > <Text> Service 6 </Text> </View>
            </View>

            <Button
                title={ I18n.t('logout') }
                buttonStyle={styles.buttonStyle}
                onPress={this.handleLogout}
            />
        </ScrollView>
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    phonenumber: '----', //state.login.data.phonenumber,
    username: '----', //state.login.data.username
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    logout: () => dispatch(LoginActions.logout())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProfileScreen)
