import React, { Component } from 'react'
import { View, TouchableOpacity, Image } from 'react-native'
import { SearchBar, Text, Button} from 'react-native-elements'
import { Container,
  Icon,
  Content,
  Input,
  Item,
  Form } from "native-base";
import I18n from '../../assets/I18n';
import { Images, Metrics } from '../../Themes'
// Styles
import styles from './Styles/NewBookingScreen'
// components
import Spacer from '../../Components/Spacer';
import AutocompleteSearch from '../../Components/AutocompleteSearch';

const translate = key => I18n.t(`screens.bookings.${key}`);
const data = [
  'data 1',
  'data 2'
];
class HomeScreen extends Component {
   state = {
     hasBooking: false,
   }

  handleGoClick = () => {
    const { navigation: { navigate } } = this.props;
    navigate('StoreSearchList')
  }

  render () {
    const { navigation } = this.props;

    return (
      <View style={styles.container}>
        <Text style={styles.title}>{translate('create')}</Text>

        <Spacer size={20} />

        <AutocompleteSearch
          placeholder={translate('searchByShop')}
          iconRight={'search'}
          iconLeft={Images.store}
          navigation={navigation}
        />
        
        <Spacer size={10} />

        <Text style={styles.or}>{translate('or')}</Text>

        <Spacer size={10} />

        <AutocompleteSearch
          placeholder={translate('searchByArea')}
          iconRight={'search'}
          iconLeft={Images.position}
          navigation={navigation}
        />

        <Spacer size={10} />

        <Text style={styles.or}>{translate('or')}</Text>
        
        <Spacer size={10} />
        
        <AutocompleteSearch
         placeholder={translate('searchSelect')}
         iconRight={'arrow-down'}
         iconLeft={Images.store}
         navigation={navigation}
        />

        <Spacer size={10} />

        <Button
            
            title={ translate('go') }
            style={styles.button}
            buttonStyle={styles.buttonStyle}
            onPress={this.handleGoClick}
        />
      </View>
    )
  }
}

export default HomeScreen
