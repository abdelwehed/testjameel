import { StyleSheet } from 'react-native'
import { Metrics, ApplicationStyles } from '../../../Themes'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  container: {
    paddingBottom: Metrics.baseMargin
  },
  listItem: {
    borderBottomColor: '#0f93cd',
    borderBottomWidth: 1
  },
  item: {
    flexDirection: 'row'
  },
  avatarContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingRight: 10,
    flex: 1.5
  },
  avatar: {
    width: 20,
    height: 20
  },
  body: {
    flexDirection: 'column',
    flex: 6,
    paddingRight: 20
  },
  bodyLeft: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  bodyRight: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  image: {
    width: 20,
    height: 20
  }
})
