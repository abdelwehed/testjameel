import { StyleSheet } from 'react-native'
import { Metrics, ApplicationStyles } from '../../../Themes'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  container: {
    paddingBottom: Metrics.baseMargin,
    flex: 1,
    justifyContent: 'center',
    paddingRight: 30,
    paddingTop: 30,
    paddingLeft: 30
  },
  title: {
    alignItems: 'flex-start',
    fontWeight: 'bold',
    fontSize: 20
  },
  ListItem: {
    borderBottomColor: '#0f93cd',
    borderBottomWidth: 1
  },
  item: {
    flexDirection: 'row'
  },
  body: {
    flexDirection: 'column',
    paddingLeft: 25
  },
  right: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  imageBackground: {
    width: 30,
    height: 30,
    marginRight: 10,
    alignItems: 'center',
    justifyContent: 'center'
  },
  text: {
    fontSize: 10
  }
})
