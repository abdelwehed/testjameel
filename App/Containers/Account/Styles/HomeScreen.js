import { StyleSheet } from 'react-native'
import { Metrics, ApplicationStyles } from '../../../Themes'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  mainContainer: {
    flex: 1,
    justifyContent: 'center'
  },
  container: {
    paddingBottom: Metrics.baseMargin
  },
  containerWithBooking: {
    flex: 1,
    marginTop: 30,
    alignItems: 'center',
    justifyContent: 'center'
  },
  imageContainer: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  image: {
    height: Metrics.images.logo,
    width: Metrics.images.logo
  }
})
