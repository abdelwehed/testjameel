import { StyleSheet } from 'react-native'
import { Metrics, ApplicationStyles } from '../../../Themes'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  container: {
    paddingBottom: Metrics.baseMargin,
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginTop: 40
  },
  title: {
    fontSize: 30,
    fontWeight: 'bold'
  },
  or: {
    fontSize: 20,
    fontWeight: 'bold'
  },
  button: {
    width: Metrics.screenWidth - 80
  }
})
