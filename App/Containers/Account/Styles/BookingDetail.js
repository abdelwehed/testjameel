import { StyleSheet } from 'react-native'
import { Metrics, ApplicationStyles } from '../../../Themes'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  container: {
    paddingBottom: Metrics.baseMargin
  },
  listItem: {
    justifyContent: 'space-between',
    borderBottomColor: 'transparent',
    borderBottomWidth: 1,
    marginRight: 20,
    paddingBottom: 40,
    paddingTop: 20
  },
  buttonsBlock: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  buttonWidth: {
    width: Metrics.screenWidth / 2.5
  }
})
