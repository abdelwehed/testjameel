import { StyleSheet } from 'react-native'
import { Metrics, ApplicationStyles } from '../../../Themes'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  container: {
    paddingBottom: Metrics.baseMargin
  },
  button: {
    borderWidth: 1,
    borderColor: '#0f93cd',
    borderRadius: 7,
    height: 50,
    marginTop: 20,
    margin: 30,
    alignItems: 'center',
    justifyContent: 'center'
  },
  buttonText: {
    fontWeight: 'bold',
    fontSize: 20
  }
})
