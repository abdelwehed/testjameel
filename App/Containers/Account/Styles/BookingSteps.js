import { StyleSheet } from 'react-native'
import { Metrics, ApplicationStyles } from '../../../Themes'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  container: {
    paddingBottom: Metrics.baseMargin
  },
  body: {
    flex: 3.5,
    justifyContent: 'center',
    alignItems: 'center'
  },
  title: {
    fontWeight: 'bold',
    fontSize: 25
  },
  timeSelector: {
    width: Metrics.screenHeight / 6,
    flex: 0.8,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  bottomText: {
    fontSize: 12,
    color: 'gray',
    textAlign: 'center'
  },
  bottom: {
    flex: 1,
    justifyContent: 'center'
  }
})
