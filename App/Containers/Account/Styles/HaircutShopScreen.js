import { StyleSheet } from 'react-native'
import { Metrics, ApplicationStyles } from '../../../Themes'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  container: {
    paddingBottom: Metrics.baseMargin,
    flex: 1,
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  image: {
    width: Metrics.screenWidth,
    height: 200,
    flex: 4
  },
  details: {
    width: Metrics.screenWidth,
    flex: 1,
    justifyContent: 'center',
    padding: 45
  },
  button: {
    flex: 1,
    width: Metrics.screenWidth - 60
  }
})
