import { StyleSheet } from 'react-native'
import { ApplicationStyles, Metrics, Colors } from '../../../Themes'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  container: {
    flex:1,
    flexDirection:'row',
    alignItems:'center',
    //justifyContent:'center'
  },
  row: {
    flex: 1,
    marginVertical: Metrics.smallMargin,
    alignSelf: 'center',
    justifyContent: 'center',
    margin: 5,
    padding: 10,
    paddingVertical: 5,
  },
  boldLabel: {
    fontWeight: 'bold',
    alignSelf: 'center',
    color: Colors.blacks,
    textAlign: 'center',
    marginBottom: Metrics.smallMargin
  },
  iconStyle: {
    resizeMode: 'contain',
    alignSelf: 'center',
    marginBottom: Metrics.smallMargin,
    height: 50,
    width: 50
  },
  label: {
    textAlign: 'center',
    color: Colors.blacks
  },
  listContent: {
    flex: 1,
    marginTop: Metrics.baseMargin
  },
  icon: {
    height: 30,
    width: 30,
    textAlign: 'center'
  },
})