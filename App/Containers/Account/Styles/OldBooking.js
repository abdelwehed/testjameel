import { StyleSheet } from 'react-native'
import { Metrics, ApplicationStyles } from '../../../Themes'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  container: {
    paddingBottom: Metrics.baseMargin
  },
  ListItem: {
    borderBottomColor: '#0f93cd',
    borderBottomWidth: 1
  },
  item: {
    flex: 1,
    height: 40,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  right: {
    flex: 0.6,
    marginLeft: 10,
    flexDirection: 'row',
    justifyContent: 'space-around'
  },
  rightIcon: {
    width: 20,
    height: 20
  },
  textSize: {
    fontSize: 12
  }
})
