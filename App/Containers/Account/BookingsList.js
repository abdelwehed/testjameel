import React, { Component } from 'react'
import { connect } from 'react-redux'
import { ScrollView, Image, View, TouchableOpacity, ActivityIndicator } from 'react-native'
import { List, ListItem, Text, Body, Right, Thumbnail } from 'native-base'
import { Images } from '../../Themes'
// Styles
import styles from './Styles/OldBooking'
// components
import IconTextItem from '../../Components/screensComponents/iconTextItem';
import BookingsActions from '../../Redux/GetBookingsRedux';

class OldBooking extends Component {
  componentDidMount = () => {
    const { navigation } = this.props;
    const currentOrOld = navigation.getParam('state');

    if(currentOrOld === 'current') this.props.getBookingsList('[after]=' + '2018-09-29');
    if(currentOrOld === 'old') this.props.getBookingsList('[before]=' + '2018-09-29');
  }

  handleShopClick = (el) => {
    const { navigation: { navigate } } = this.props;
    navigate('BookinDetailsScreen', { booking: el });
  }

  renderList = (bookings) => {
    return(
      <View>
     {bookings.map((el, i) => {
      return (
       <List key={i}>
       <ListItem style={styles.ListItem}>
        <TouchableOpacity onPress={() => this.handleShopClick(el)} style={styles.item} >
                <IconTextItem
                text={el.startAt.slice(0, 10)}
                icon={Images.calendar}
                iconStyle={{width: 15, height: 15}}
                textStyle={styles.textSize}
                displayText
                el={el} />

                <IconTextItem
                text={el.startAt.slice(11, 16)}
                icon={Images.watch}
                iconStyle={{width: 18, height: 18}}
                textStyle={styles.textSize}
                displayText />
                
              <IconTextItem
              text={"barber name"}
              icon={Images.store}
              iconStyle={{width: 12, height: 12}}
              textStyle={styles.textSize}
              displayText />

              <View style={styles.right}>
                <Image 
                source={{uri: Images.greenCircle}}
                style={styles.rightIcon}
                />
                <Image 
                source={{uri: Images.arroRight}}
                style={styles.rightIcon}
                />
              </View>
         </TouchableOpacity>
       </ListItem>
     </List>
      )
    })}
    </View>
   );
  }

  render () {
    const { getdBookingsFetching, BookingsList } = this.props;

    if(getdBookingsFetching) return <ActivityIndicator />

    return (
      <View style={styles.mainContainer}>
        <Image source={Images.background} style={styles.backgroundImage} resizeMode='stretch' />
        <ScrollView
        showsVerticalScrollIndicator={false}
        style={styles.container}>
        {this.renderList(BookingsList!==null ? BookingsList : [])}
        </ScrollView>
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    BookingsList: state.BookingsList.bookings,
    getdBookingsFetching: state.BookingsList.fetching
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getBookingsList: (date) => dispatch(BookingsActions.getBookingsRequest(date)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(OldBooking)

