import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Image, View, Text, ActivityIndicator } from 'react-native'
import { Button } from 'react-native-elements'
import I18n from '../../assets/I18n'
import { Images } from '../../Themes'
import ProfileActions from '../../Redux/ProfileRedux';
// Styles
import styles from './Styles/BookingScreenStyle'
// components
import BarberProfile from '../../Components/BarberProfile';

const data = [{key: 'a', name: 'Service 1', price: '$30', time: '10 min'}, {key: 'b', name: 'Service 2', price: '$30', time: '10 min'},
      {key: 'c', name: 'Service 3', price: '$30', time: '10 min'},{key: 'd', name: 'Service 4', price: '$30', time: '10 min'},
      {key: 'e', name: 'Service 5', price: '$30', time: '10 min'},{key: 'f', name: 'Service 6', price: '$30', time: '10 min'},
      {key: 'g', name: 'Service 7', price: '$30', time: '10 min'}, {key: 'h', name: 'Service 8', price: '$30', time: '10 min'}];

const translate = key => I18n.t(`components.booking.${key}`);

class AccountScreen extends Component {
  handleNextClick = () => {
    const { navigation: { navigate } } = this.props;
    navigate('BookingStepTwo');
  }

  componentDidMount = () => {
    this.props.getprofile('1');
  }

  render () {
    const { profileInfos, profileFetching } = this.props;

    if(profileFetching) return <ActivityIndicator />

    return (
      <View style={styles.mainContainer}>
        <Image source={Images.background} style={styles.backgroundImage} resizeMode='stretch' />
        <BarberProfile
        photo={profileInfos!==null && profileInfos.photo}
        name={profileInfos!== null && profileInfos.fullname}
        phone={profileInfos!== null && profileInfos.phone}
        email={profileInfos!== null && profileInfos.email}
        id={profileInfos!== null && profileInfos.id}
        />
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    profileInfos: state.profile.userInfos,
    profileFetching: state.profile.fetching
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getprofile: (userId) => dispatch(ProfileActions.getProfileRequest(userId)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AccountScreen)
