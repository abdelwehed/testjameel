import React, { Component } from 'react'
import { View,Image, AppState } from 'react-native'
import { Avatar, Icon, Text, Button} from 'react-native-elements'
import PushNotification from 'react-native-push-notification';
import I18n from '../../assets/I18n'
import { Images, Metrics } from '../../Themes'
// Styles
import styles from './Styles/HomeScreen'
//components
import CurrentBooking from '../../Components/screensComponents/currentBooking';
import Spacer from '../../Components/Spacer';
import PushControlle from './puhsNotifs';

const translate = key => I18n.t(`components.booking.${key}`);

class HomeScreen extends Component {
   state = {
     hasBooking: false,
   }

   componentDidMount = () => {
    AppState.addEventListener('change', this.handleAppStateChange);
   }

   componentWillUnmount = () => {
     AppState.removeEventListener('change', this.handleAppStateChange);
   }

   handleAppStateChange = (appState) => {
     if(appState == 'background') {
       console.log('BACKGROUND_APP')
      PushNotification.localNotificationSchedule({
        message: "yaaaay ! Notification works perfectly !!", // (required)
        date: new Date(Date.now() + (5 * 1000)) // in 60 secs
      });
     }
   }

   handleMakeBookingClick = () => {
    const { navigation: { navigate } } = this.props;
    navigate({ routeName: 'NewBookingScreen' })
  }

  render () {
    const { hasBooking } = this.state;

    return (
      <View style={styles.mainContainer}>
        <Image source={Images.background} style={styles.backgroundImage} resizeMode='stretch' />
        {!hasBooking ? <View style={styles.container}>
            <Button
                title={ translate('makeNew') }
                buttonStyle={styles.buttonStyle}
                onPress={this.handleMakeBookingClick}
            />
        </View>
      :
        <View style={styles.containerWithBooking}>
          <View style={styles.imageContainer}>
            <Image style={styles.image} source={{ uri: Images.logo }} />
          </View>

          <Spacer size={40} />

          <CurrentBooking />
        </View>}
        <PushControlle />
      </View>
    )
  }
}

export default HomeScreen
