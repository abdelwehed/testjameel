import React, { Component } from 'react'
import { View, TouchableOpacity, ScrollView, ImageBackground, Image } from 'react-native'
import { List, ListItem, Text, Body, Right, Thumbnail } from 'native-base'
import { Icon } from 'react-native-elements';
import I18n from '../../assets/I18n'
import { Images, Metrics } from '../../Themes'
import styles from './Styles/SearchByArea'

//components
import AutocompleteSearch from '../../Components/AutocompleteSearch';
import IconTextItem from '../../Components/screensComponents/iconTextItem';
import Spacer from '../../Components/Spacer';

const translate = key => I18n.t(`screens.searchByArea.${key}`);
const data = [
  '', '', '', '', '', '', '', ''
];

class SearchByArea extends Component {
   state = {
   }

   handleShopClick = () => {
    const { navigation: { navigate } } = this.props;
    navigate('HairCutShop', { shop: {id: '1'} })
  }

   renderList = () => {
     return(
       <View>
      {data.map((el, i) => {
       return (
        <List key={i}>
        <ListItem style={styles.ListItem}>
        <TouchableOpacity onPress={this.handleShopClick} style={styles.item} >
            <View>
            <Thumbnail 
            source={{uri: 'https://static4.pagesjaunes.fr/media/ugc/l_homme_02737500_235231408'}}
            />
            </View>

            <Body style={styles.body}>
              <IconTextItem
              icon={Images.store}
              iconStyle={{width: 12, height: 12}}
              displayText
              el={el} />

              <Spacer size={2} />

              <IconTextItem
              icon={Images.position}
              iconStyle={{width: 12, height: 18}}
              displayText />

              <Spacer size={2} />

              <IconTextItem
              icon={Images.stars}
              iconStyle={{width: 90, height: 12}}
              displayText={false} />
            </Body>
          

          <Right style={styles.right}>
            <ImageBackground 
            source={{uri: Images.greenCircle}}
            style={styles.imageBackground}
            >
             <Icon name="map-marker-alt" color="#fff" />
            </ImageBackground>
            <Text style={styles.text}>3.5 Km</Text>
          </Right>
          </TouchableOpacity>

        </ListItem>
      </List>
       )
     })}
     </View>
    );
   }

  render () {
    const { navigation } = this.props;

    return (
      <View style={styles.container}>
      <Image source={Images.background} style={styles.backgroundImage} resizeMode='stretch' />  
      <AutocompleteSearch
          placeholder={translate('searchByArea')}
          iconRight={'search'}
          iconLeft={Images.position}
          navigation={navigation}
        />

        <Text style={styles.title}>{translate('nearbySearch')}</Text>

        <ScrollView
        showsVerticalScrollIndicator={false}
        >
          {this.renderList()}
        </ScrollView>
      </View>
    )
  }
}

export default SearchByArea
