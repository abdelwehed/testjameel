import React, { Component } from 'react'
import { Image, View, Text } from 'react-native'
import { Button } from 'react-native-elements'
import I18n from '../../assets/I18n'
import { Images, Metrics } from '../../Themes'
// Styles
import styles from './Styles/BookingSteps'
// components
import Step from '../../Components/step';
import BookingProfile from '../../Components/BookingProfile';
import NumberSelector from '../../Components/number-selector';

const translate = key => I18n.t(`components.booking.${key}`);
const hoursData= ['01', '02', '03', '04', '05', '06', '07', '08'];
const MinutesData= ['01', '02', '03', '04', '05', '06', '07', '08'];

export default class BookingThirdStep extends Component {
  state = {
    activeIndex: 0,
    minute: null,
    hour: null,
  }

  onScrollEndDrag = (e) => {
    //console.log('scrol drag', e.nativeEvent.velocity, e.nativeEvent.contentOffset);
    const itemIndex = Math.round(e.nativeEvent.contentOffset.y / 30);
    //console.log('itemindex', itemIndex)
    this.setState({ activeIndex: itemIndex });
  }

  isTimeValide = (minute, hour) => {
    return minute !==null && hour!==null;
  }

  getTime = () => {
    const { minute, hour } = this.state;
    const valideTime = this.isTimeValide(minute, hour);

    if(valideTime) return `${hour}:${minute}`;
  }

  handleSaveBookingTunnel = () => {
    const { navigation: { navigate }, navigation } = this.props;
    const thirdBookingObject = navigation.getParam('thirdBookingObject');
    const time = this.getTime();
    const shouldNavigateToNextStep = thirdBookingObject && time;
    const provisionalBooking = {
      ...thirdBookingObject,
      startAt: `${thirdBookingObject.startAt}${time}`
    };
    console.log({ THIRD_STEP: provisionalBooking });
    shouldNavigateToNextStep && navigate('BookinDetailsScreen', { booking: provisionalBooking });
  }

  render () {
    const time = this.getTime();

    return (
      <View style={styles.mainContainer}>
        <Image source={Images.background} style={styles.backgroundImage} resizeMode='stretch' />
        <BookingProfile />

        <View style={styles.body}>
          <Text style={styles.title}>{translate('time-selection')}</Text>

          <View style={styles.timeSelector}>
            <NumberSelector
              onSelect={(selectedHour) => { this.setState({ hour: selectedHour}) }}
              title="HH"
              data={hoursData}
            />
            <Text style={{ fontWeight: 'bold', fontSize: 20 }} >:</Text>
            <NumberSelector
              onSelect={(selectedMinute) => { this.setState({ minute: selectedMinute}) }}
              title="MM"
              data={MinutesData}
            />
          </View>

          <Text style={styles.bottomText}>{translate('availability')}</Text>
        </View>

        <View style={styles.bottom}>
          <Button
              disabled={!time}
              title={ translate('next') }
              buttonStyle={styles.buttonStyle}
              onPress={this.handleSaveBookingTunnel}
          />
          <Step step='3' />
        </View>
      </View>
    )
  }
}