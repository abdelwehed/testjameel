import React, { Component } from 'react'
import { Image, View, TouchableOpacity, AsyncStorage } from 'react-native'
import { ListItem } from 'native-base'
import { Button} from 'react-native-elements'
import { connect } from 'react-redux'
import DialogInput from 'react-native-dialog-input'
import AddBookingActions from '../../Redux/AddBookingRedux';
import I18n from '../../assets/I18n'
import { Images, Metrics } from '../../Themes'
// Styles
import styles from './Styles/BookingDetail'
// components
import IconTextItem from '../../Components/screensComponents/iconTextItem';
import Spacer from '../../Components/Spacer'

const translate = key => I18n.t(`components.booking.${key}`);
class BookingDetailsScreen extends Component {
  state = {
    isSavedBooking: false,
    toggleDialog: false
  }
  renderTopBlock = (date, shop, barber) => {
    return(
       <ListItem style={{ borderBottomColor: '#0f93cd', borderBottomWidth: 1, marginRight: 20, paddingBottom: 20, paddingTop: 20 }}>
       <TouchableOpacity onPress={this.handleShopClick} style={{flexDirection: 'row', alignItems: 'center'}}>
             <View style={{flexDirection: 'column', flex: 1.5}}>
                <IconTextItem
                icon={Images.store}
                iconStyle={{width: 15, height: 15}}
                displayText
                text={shop.name ? shop.name : shop} />

                <Spacer size={15} />

                <IconTextItem
                icon={Images.calendar}
                iconStyle={{width: 12, height: 12}}
                displayText
                text={date.slice(0, 10)} />
             </View>

             <View style={{flexDirection: 'column', flex: 1}}>
              <IconTextItem
              icon={Images.cut}
              iconStyle={{width: 20, height: 20}}
              displayText
              text={barber.fullname ? barber.fullname : barber} />

              <Spacer size={15} />

              <IconTextItem
              icon={Images.calendar}
              iconStyle={{width: 12, height: 12}}
              displayText
              text={date.slice(11, 16)} />
             </View>
         </TouchableOpacity>
       </ListItem>
   );
  }

  renderMiddleBlock = (service) => {
    const serviceName = typeof service==='string' ? service : service.label;
    const servicePrice = typeof service==='string' ? 'No Price' : service.price;
    return(
       <ListItem style={{ borderBottomColor: '#0f93cd', borderBottomWidth: 1, marginRight: 20, paddingBottom: 20, paddingTop: 20 }}>
       <TouchableOpacity onPress={this.handleShopClick} style={{flexDirection: 'row'}}>
             <View style={{flexDirection: 'column', flex: 7.5}}>
                <IconTextItem
                icon={Images.blueCircle}
                iconStyle={{width: 15, height: 15}}
                displayText
                textStyle={{fontWeight: 'bold'}}
                text={serviceName} />
             </View>

             <View style={{flexDirection: 'column', flex: 1, justifyContent: 'flex-end'}}>
              <IconTextItem
              displayText
              textStyle={{fontWeight: 'bold'}}
              text={servicePrice} />
             </View>
         </TouchableOpacity>
       </ListItem>
   );
  }

  handleSaveBookingClick = () => {
    // on doit rederiger vers currentBookingsList - reste à implemnter current list pour pouvoir ajouter la redirection
    const { navigation: { navigate }, navigation } = this.props;
    const { toggleDialog, isSavedBooking } = this.state;
    const booking = navigation.getParam('booking');
    const { service: { name }, startAt, store, barber } = booking;

    const myBooking = {
      service: name,
      store: `/stores/${store.id}`,
      barber: `${barber.id}`,
      user: "/users/2",
      startAt: `${startAt}T${startAt.slice(11, 16)}+00:00`,
    };

   this.props.saveBooking(myBooking);
/*    console.log({ booking_OUT: isSavedBooking})

   if(!isSavedBooking) {
    console.log({ bookingIS_NOTSAVED: isSavedBooking})
    this.setState({ toggleDialog: true });
   }else {
    console.log({ bookinISSAVED: isSavedBooking})
    navigate('BookingsListScreen', {state: 'current'}) 
   } */
  }

  handleCancelBookingClick = () => {
    const { navigation: { navigate } } = this.props;
    navigate('NewBookingScreen');
  }

  handleUpdateBookingClick = () => {
    const { navigation: { navigate }, navigation } = this.props;
    const booking = navigation.getParam('booking');

    const { service: { name }, startAt, store, barber } = booking;

    const myBooking = {
      service: name,
      store: `/stores/${store.id}`,
      barber: `${barber.id}`,
      user: "/users/2",
      startAt: `${startAt}T${startAt.slice(11, 16)}+00:00`,
    };
    console.log({ UPDATE_BOOKING_BUTTON: myBooking });

    AsyncStorage.setItem('booking', JSON.stringify(myBooking));
    navigate('BookingStepOne');
  }

/*   static getDerivedStateFromProps = (nextProps, prevState) => {
    console.log({ nextProps: nextProps.bookingAdded, prevState: prevState.bookingAdded})

    if(nextProps.bookingAdded === prevState.bookingAdded){
      return null
    }

    return { isSavedBooking: nextProps.bookingAdded };
  } */

  render () {
    const { toggleDialog } = this.state;
    const { navigation, bookingAdded } = this.props;
    const booking = navigation.getParam('booking');

    const { service, startAt, store, barber } = booking;
    console.log({ RENDER: booking });

    return (
      <View style={styles.mainContainer}>
        <Image source={Images.background} style={styles.backgroundImage} resizeMode='stretch' />
        <View>
          {this.renderTopBlock(startAt, store, barber)}

          {this.renderMiddleBlock(service)}

          <ListItem style={styles.listItem}>
            <IconTextItem
              icon={Images.blueCircle}
              iconStyle={{width: 15, height: 15}}
              displayText
              textStyle={{fontWeight: 'bold'}}
              text="TOTAL" />

              <IconTextItem
              displayText
              textStyle={{fontWeight: 'bold'}}
              text={typeof service==='string' ? 'No Price' : service.price} />
          </ListItem>
        </View>

        <Button
              title={'Save'}
              buttonStyle={styles.buttonStyle}
              onPress={() => this.handleSaveBookingClick(bookingAdded!==null && bookingAdded)}
        />

        <View style={styles.buttonsBlock}>
          <Button
            title={'Update'}
            buttonStyle={[styles.buttonStyle, styles.buttonWidth]}
            onPress={this.handleUpdateBookingClick}
          />
          <Button
            title={'Cancel'}
            buttonStyle={[styles.buttonStyle, styles.buttonWidth]}
            onPress={this.handleCancelBookingClick}
          />
        </View>
        <DialogInput isDialogVisible={toggleDialog}
        title={"Make a booking"}
        modalStyle={{backgroundColor: 'black', opacity: 0.8}}
        dialogStyle={{backgroundColor: '#fff'}}
        message={"Please verify your booking informations or make a new booking"}
        hintInput ={"Error saving your booking !"}
        submitInput={ (inputText) => this.sendInput(inputText, id) }
        closeDialog={ () => { this.setState({ toggleDialog: false, emptyInput: null }) } }>
     </DialogInput>
      </View>
    )
  }
}


const mapStateToProps = (state) => {
  return {
    bookingAdded: state.addBooking.wellSaved,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    saveBooking: (params) => dispatch(AddBookingActions.addBookingRequest(params)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(BookingDetailsScreen)
